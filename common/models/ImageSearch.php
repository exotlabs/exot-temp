<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Image;

/**
 * ImageSearch represents the model behind the search form of `common\models\Image`.
 */
class ImageSearch extends Image
{
    public $search;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'width', 'height', 'created_by', 'created_at'], 'integer'],
            [['file_name'], 'safe'],
            ['search', 'string', 'min' => 1],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Image::find()->orderBy([
            'created_at' => SORT_DESC
          ]);
        $this->load($params);
        if(!empty($this->search)){
            $query->andFilterWhere(['ilike', 'file_name', $this->search]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;

        
        
       // $this->load($params);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'width' => $this->width,
        //     'height' => $this->height,
        //     'created_by' => $this->created_by,
        //     'created_at' => $this->created_at,
        // ]);
        
        
    }
}
