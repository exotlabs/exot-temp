<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property string $file_name
 * @property int $width
 * @property int $height
 * @property int $created_by
 * @property int $updated_by
 * @property int $created_at
 * @property int $updated_at
 */
class Image extends \yii\db\ActiveRecord
{
    private $folder;
    private $path;
    public $files;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_name', 'width', 'height'], 'required'],
            [['width', 'height', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['width', 'height', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['file_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'Fájlnév',
            'width' => 'Szélesség',
            'height' => 'Magasság',
            'created_by' => 'Létrehozta',
            'updated_by' => 'Módosította',
            'created_at' => 'Feltöltve',
            'updated_at' => 'Módosítva',
        ];
    }

    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            FileHelper::removeDirectory($this->getFolder());
            return true;
        }
        return false;
    }

    /**
     * Create and set valid file name, from original file name
     *
     * @param [type] $originalFileName 
     * @return string new file ame
     */
    public function setFileName($originalFileName){
        $path_info = pathinfo($originalFileName);
        $filename = preg_replace("/[^a-z0-9.]+/i", "_", $path_info['filename']);
        $this->file_name = $filename . '.' . $path_info['extension'];
    }

    /**
     * Get the relative path from uploadFolder 
     *
     * @return string path
     */
    public function getPath(){
        if(empty($this->id) || empty($this->file_name)){
            return false;
        }
        return $this->getFolder(true) . '/' . $this->file_name;    
    }
    
    /**
     * Get the relative path from uploadFolder 
     *
     * @return string path
     */
    public function getFolder($absoulte = true){
        if(empty($this->id)){
            return false;
        }
        $this->folder = substr_replace(md5($this->id), '/', 2, 0);

        if($absoulte){
            return realpath(Yii::getAlias('@frontend')) . '/web' . Yii::$app->params['uploadFolder'] . $this->folder;
        }
        return $this->folder;
    }

    /**
     * Get image URL use imagereize plugin
     *
     * @param [type] $width
     * @param [type] $height
     * @return string url
     */
    public function getUrl($width = null, $height = null){
        if(empty($width) || $width > $this->width){
            $width = $this->width;
        }
        if(empty($height) || $height > $this->height){
            $width = $this->height;
        }
        
        if(file_exists($this->getPath())){
            return Yii::$app->urlManager->getHostInfo() . Yii::$app->imageresize->getUrl($this->getPath(), $width, $height, "inset", 80);
        }

        return Yii::$app->params['nopicPath'];
    }
}
