<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Article;

/**
 * ArticleSearch represents the model behind the search form of `common\models\Article`.
 */
class ArticleSearch extends Article
{
    public $search;
    public $relatedId;
    public $notRelatedId;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'published_user_id', 'published_at', 'created_at', 'updated_at', 'type'], 'integer'],
            [['title', 'lead', 'description'], 'safe'],
            ['search', 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder'=> [
                    'created_at' => SORT_DESC
                ]
            ]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'type' => $this->type,
            'published_user_id' => $this->published_user_id,
            'published_at' => $this->published_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'title', $this->title])
            ->andFilterWhere(['ilike', 'lead', $this->lead])
            ->andFilterWhere(['ilike', 'description', $this->description]);

        if (!empty($this->relatedId)) {
            $subQuery = (new \yii\db\Query)
                ->select([new \yii\db\Expression('1')])
                ->from('article_related ar')
                ->where('article.id = ar.related_id')
                ->andWhere(['ar.article_id' => $this->relatedId]);
            $query->andWhere(['exists',$subQuery]);
            $query->andWhere(['!=','id',$this->relatedId]);
        }
        $query->andFilterWhere(['ilike','title',$this->search]);

        if (!empty($this->notRelatedId)) {
            $subQuery = (new \yii\db\Query)
                ->select([new \yii\db\Expression('1')])
                ->from('article_related ar')
                ->where('article.id = ar.related_id')
                ->andWhere(['ar.article_id' => $this->notRelatedId]);
            $query->andWhere(['not exists',$subQuery]);
            $query->andWhere(['!=','id',$this->notRelatedId]);
        }
        return $dataProvider;
    }
}
