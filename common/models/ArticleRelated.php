<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "article_related".
 *
 * @property int $id
 * @property int $article_id
 * @property int $related_id
 * @property int $created_user_id
 * @property int $created_at
 *
 * @property Article $article
 * @property Article $related
 * @property User $createdUser
 */
class ArticleRelated extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_related';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['article_id', 'related_id', 'created_user_id', 'created_at'], 'required'],
            [['article_id', 'related_id', 'created_user_id', 'created_at'], 'default', 'value' => null],
            [['article_id', 'related_id', 'created_user_id', 'created_at'], 'integer'],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
            [['related_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['related_id' => 'id']],
            [['created_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_id' => 'Article ID',
            'related_id' => 'Related ID',
            'created_user_id' => 'Created User ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelated()
    {
        return $this->hasOne(Article::className(), ['id' => 'related_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_user_id']);
    }
}
