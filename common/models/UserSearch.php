<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form of `common\models\User`.
 *  @property string $search
 */
class UserSearch extends User
{
    public $name;
    public $search;
    public $created_at_from_to;


    public $listGroup = self::LIST_GROUP_USER;

    const LIST_GROUP_ADMINS = 1;
    const LIST_GROUP_USER = 3;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'gender', 'age', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'auth_key', 'name', 'password_hash', 'password_reset_token', 'email', 'first_name', 'family_name'], 'safe'],
            ['search', 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder'=> [
                    'created_at' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);
        $fromTo = explode(' - ', $this->created_at_from_to);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gender' => $this->gender,
            'age' => $this->age,
            'status' => $this->status == -1?null:$this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'username', $this->username])
            ->andFilterWhere(['ilike', 'email', $this->email])
            ->andFilterWhere(['ilike', 'first_name', $this->first_name])
            ->andFilterWhere(['ilike', 'family_name', $this->family_name])
            ->andFilterWhere(['ilike', 'concat(first_name,family_name)', $this->name]);

        if (is_array($fromTo) && count($fromTo)===2) {
            $query->andWhere(['between', 'created_at', strtotime($fromTo[0]), strtotime($fromTo[1])]);
        }

        $query->andFilterWhere(['ilike','concat(username,email,first_name,family_name)',$this->search]);

        switch ($this->listGroup) {
            case self::LIST_GROUP_ADMINS:
                $query->andFilterWhere(['exists', self::adminSubQuery()]);
                break;
            // list application users as default
            case self::LIST_GROUP_USER:
            default:
                $query->andFilterWhere(['exists', self::userSubQuery()]);
                break;
        }

        return $dataProvider;
    }

    public static function adminSubQuery(){
        return (new \yii\db\Query)
            ->select([new \yii\db\Expression('1')])
            ->from('auth_assignment aa')
            ->where('"user".id::varchar = aa.user_id')
            ->andWhere(['aa.item_name' => User::getAdminRoles()]);
    }

    public static function userSubQuery(){
        return (new \yii\db\Query)
            ->select([new \yii\db\Expression('1')])
            ->from('auth_assignment aa')
            ->where('"user".id::varchar = aa.user_id')
            ->andWhere(['aa.item_name' => User::getNormalRoles()]);
    }

}
