<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $title
 * @property string $lead
 * @property string $description
 * @property int $status
 * @property int $published_user_id
 * @property int $published_at
 * @property int $created_at
 * @property int $updated_at
 * @property int $type
 *
 * @property User $publishedUser
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * Type constants
     */
    const TYPE_NEWS = 1;
    const TYPE_CONTENT = 2;

    /**
     * Type constants
     */
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $click_url;
    public $recommandation_image_url;

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            /*
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            */
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'lead', 'published_user_id', 'published_at', 'type'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['status', 'published_user_id', 'published_at', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['status', 'published_user_id', 'created_at', 'updated_at', 'type', 'image_id'], 'integer'],
            [['title', 'lead'], 'string', 'max' => 255],
            [['published_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['published_user_id' => 'id']],
            ['published_at', 'datetime', 'timestampAttribute' => 'published_at', 'format' => 'yyyy-MM-dd HH:mm:ss'],
            ['published_at', 'default', 'value' => time()],
            [['publishedDate', 'recommandation_image_url', 'click_url'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Cím',
            'lead' => 'Lead',
            'description' => 'Tartalom',
            'status' => 'Státusz',
            'published_user_id' => 'Szerző',
            'published_at' => 'Publikálva',
            'created_at' => 'Létrehozás dátuma',
            'updated_at' => 'Módosítva',
            'type' => 'Típus',
            'image_id' => 'Ajánlókép'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'published_user_id']);
    }

    public function getRelatedArticles()
    {
        return $this->hasMany(Article::className(), ['id' => 'related_id'])
            ->viaTable('article_related', ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Image::className(), ['id' => 'image_id']);
    }

    /**
     * Return type label text
     *
     * @return mixed|string
     */
    public function getTypeLabel() {
        if (isset($this->type) && array_key_exists($this->type, self::getTypeList())) {
            return self::getTypeList()[$this->type];
        } else {
            return "";
        }
    }

    /**
     * Return the type list options
     *
     * @return array
     */
    public static function getTypeList()
    {
        return [
            self::TYPE_NEWS => 'Hír',
            self::TYPE_CONTENT => 'Exkluzív tartalom'
        ];
    }

    /**
     * Return status label text
     *
     * @return mixed|string
     */
    public function getStatusLabel() {
        if (isset($this->status) && array_key_exists($this->status, self::getStatusList())) {
            return self::getStatusList()[$this->status];
        } else {
            return "";
        }
    }

    /**
     * Return the type list options
     *
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Éles',
            self::STATUS_INACTIVE => 'Inaktív'
        ];
    }

    /**
     * Return the formatted published date
     *
     * @return false|string
     */
    public function getPublishedDate()
    {
        return date("Y-m-d H:i:s", $this->published_at);
        //return Yii::$app->formatter->asDate(($this->isNewRecord) ? time() : $this->published_at);
    }

    /**
     * Sets the formatted date
     *
     * @param string $date The string date
     */
    public function setPublishedDate($date)
    {
        $this->published_at = (new \DateTime($date))->getTimestamp();
    }

}
