<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;


/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_NO_SELECTED = -1;
    const STATUS_DELETED = 0;
    const STATUS_PENDING = 5; // Not confirmed user
    const STATUS_ACTIVE = 10;

    /**
     * Role constants
     */
    const ROLE_DEFAULT = 'defaultRole'; // authManager's role
    const ROLE_ADMIN = "admin";
    const ROLE_USER = "user";

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    const SCENARIO_LOGIN = 'login';
    const SCENARIO_SIGNUP = 'signup';
    const SCENARIO_MODIFY = 'modify';
    const SCENARIO_ADMIN_CREATE = 'adminCreate';
    const SCENARIO_ADMIN_UPDATE = 'adminUpdate';
    const SCENARIO_REQUEST_PASSWORD_RESET = 'requestPasswordReset';
    const SCENARIO_CHECK_PASSWORD = 'checkPassword';
    const SCENARIO_DEFAULT = 'default';  // for rest api modifications

    // Transient fields
    public $passwordPlain;
    public $passwordConfirm;
    public $selectedAuthItem;

    public $subaction;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }


    public function attributeLabels()
    {
        return [
            "first_name" => "Keresztnév",
            "family_name" => "Vezetéknév",
            "fullName" => 'Teljes név',
            "gender" => "Nem",
            "age" => "Kor",
            "status" => "Státusz",
            "passwordPlain" => "Jelszó",
            "passwordConfirm" => "Jelszó újra",
            "selectedAuthItem" => "Jogosultság",
            "created_at" => "Létrehozva",
            "updated_at" => "Módosítva",
            "zip" => "Irányítószám"
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_LOGIN => ['email', 'passwordPlain'],
            self::SCENARIO_ADMIN_CREATE =>  ['email', 'first_name','family_name', 'gender', 'age','passwordPlain', 'passwordConfirm', 'selectedAuthItem', 'status'],
            self::SCENARIO_ADMIN_UPDATE =>  ['email', 'first_name','family_name', 'gender', 'age','passwordPlain', 'passwordConfirm', 'selectedAuthItem', 'status'],
            self::SCENARIO_SIGNUP =>  ['email', 'passwordPlain', 'first_name','family_name', 'gender', 'age', 'zip'],
            self::SCENARIO_MODIFY => ['email_new', 'passwordPlain', 'first_name','family_name', 'gender', 'age', 'zip', 'passwordOld', 'subaction'],
            self::SCENARIO_REQUEST_PASSWORD_RESET  => ['email'],
            self::SCENARIO_CHECK_PASSWORD  => ['passwordPlain'],
            self::SCENARIO_DEFAULT =>  ['email', 'first_name','family_name', 'gender', 'age'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['confirm_token','first_name','family_name'], 'string'],
            [['gender','age','zip'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Finds user by confirm reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findPendingUserByConfirmToken($token)
    {
        return static::findOne([
            'confirm_token' => $token,
            'status' => self::STATUS_PENDING,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Generates new confirmation token
     */
    public function generateConfirmToken()
    {
        $this->confirm_token = $this->replaceUnwanted(Yii::$app->security->generateRandomString()) . '_' . time();
    }

    /**
     * Replace special chars
     *
     * @param string $input The input string
     *
     * @return mixed
     */
    public function replaceUnwanted($input){
        return str_replace('-','',$input);
    }

    /**
     * Return the user's full name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->username;
    }

    /**
     * Return the gender list options
     *
     * @return array
     */
    public static function getGenderList()
    {
        return [
            self::GENDER_FEMALE => 'Nő',
            self::GENDER_MALE => 'Férfi'
        ];
    }

    /**
     * Return the role list options
     *
     * @return array
     */
    public static function getAdminRoles()
    {
        return [
            self::ROLE_ADMIN,
        ];
    }

    /**
     * Return the role list options
     *
     * @return array
     */
    public static function getAdminRoleList()
    {
        return [
            self::ROLE_ADMIN => 'Admin',
        ];
    }

    /**
     * Return the role list options
     *
     * @return array
     */
    public static function getNormalRoles()
    {
        return [
            self::ROLE_USER,
        ];
    }

    /**
     * Return the role list options
     *
     * @return array
     */
    public static function getNormalRoleList()
    {
        return [
            self::ROLE_USER => self::ROLE_USER,
        ];
    }

    /**
     * Return status label text
     *
     * @return mixed|string
     */
    public function getStatusLabel() {
        if (isset($this->status) && array_key_exists($this->status, self::getStatusList())) {
            return self::getStatusList()[$this->status];
        } else {
            return "";
        }
    }

    /**
     * Return the status list options
     *
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_DELETED => 'Törölt',
            self::STATUS_ACTIVE => 'Aktív',
        ];
    }

    /**
     * Return the status list options
     *
     * @return array
     */
    public static function getStatusListAndEmpty()
    {

        $arr = [ self::STATUS_NO_SELECTED => ' Válasz státuszt!'] + self::getStatusList();

        return $arr;
    }

    /**
     * Get the role name for the user
     *
     * @return name of the role, or null
     */
    public function getRolesNames(){
        $role = Yii::$app->authManager->getRolesByUser($this->id);
        if(!empty($role)){
            return array_keys($role);
        }
        return null;
    }

    /**
     * Get the role name for the user
     *
     * @return name of the role, or null
     */
    public function getFirstRoleName(){
        $rolenames = $this->getRolesNames();
        ArrayHelper::removeValue($rolenames,self::ROLE_DEFAULT);
        $rolenames = array_values($rolenames);
        if(!empty($rolenames)){
            return $rolenames[0];
        }
        return null;
    }

    /**
     * Get the role name for the user as text
     *
     * @return name of the role, or null
     */
    public function getFirstRoleNameText()
    {
        $code = $this->getFirstRoleName();
        $admins = self::getAdminRoleList();

        if (isset($code) && in_array($code, array_keys($admins))) {
            return $admins[$code];
        }

        return null;
    }

}
