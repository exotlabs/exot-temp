<?php

namespace common\components;

use Yii;
use yii\grid\GridView;
use yii\helpers\Html;

/**
 *
 */
class ExotGridView extends GridView
{

    public $layout = "{items}<div class=\"datatable-footer\"><div class=\"dataTables_info\">{summary}</div><div class=\"dataTables_paginate paging_simple_numbers\">{pager}</div></div>";

    public $tableOptions = [
        'class' => 'table datatable-basic dataTable no-footer'
    ];

    public function __construct(array $config = [])
    {

        if (isset($config["options"])) {
            foreach ($config["options"] as $idx => $optionValue) {
                $this->options[$idx] = $optionValue;
            }
        }

        parent::__construct($config);
    }

    public static function exotButtons() {
        return [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Actions',
            'template' => '<div class="list-icons">
                <div class="dropdown">
                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                        <i class="icon-menu9"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        {update}
                        {delete}
                    </div>
                </div>
            </div>',
            'buttons' =>
            [
                'update'=>function($url,$model,$key)
                {
                    return Html::a( "<i class=\"icon-pencil \"></i> Edit" , $url, [
                        'class' => 'dropdown-item',
                        ] ); //use Url::to() in order to change $url
                },
                'view'=>function($url,$model,$key)
                {
                    return Html::a( "<i class=\"icon-newspaper\"></i> View" , $url, [
                        'class' => 'dropdown-item',
                        ] ); //use Url::to() in order to change $url
                },
                'delete'=>function($url,$model,$key)
                {
                    return '<form action="'.$url.'" method="post"><input type="hidden" name="id" value="'.$key.'">'
                            . '<input type="hidden" name="'.Yii::$app->request->csrfParam.'" value="'.Yii::$app->request->csrfToken.'">'
                            . '<button type="submit" class="dropdown-item delete-item"><i class="icon-trash"></i> Delete</button></form>';
                }
            ],
        ];
    }
}


 ?>
