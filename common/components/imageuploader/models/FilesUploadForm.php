<?php

namespace common\components\imageuploader\models;

use Yii;
use yii\base\Model;
use common\models\Image;

class FilesUploadForm extends Model
{
    public $files;
    public $uploadedFiles;

    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['files'], 'safe'],
            [['files'], 'file', 'extensions'=>'jpeg, jpg, gif, png', 'maxFiles' => 10],   ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
        ];
    }

    /**
     * Upload images
     *
     * @return void
     */
    public function upload()
    {
        foreach($this->uploadedFiles as $file){            
            $transaction = Yii::$app->db->beginTransaction();

            try {
                $image = new Image();
                $size = getimagesize($file->tempName);
                if(empty($size)){
                    return false;
                }
                $image->width = $size[0];
                $image->height = $size[1];
                $image->setFileName($file->name);
                if(!$image->save()){
                    Yii::error($image->getErrors(), __METHOD__);
                    return false;
                }

                if(empty($image->folder)){
                    return false;
                }
                
                if (!file_exists($image->folder)) {
                     mkdir($image->folder, 0777, true);
                }

                if(!$file->saveAs($image->getPath())){
                    return false;
                }

                $transaction->commit();

            } catch (Exception $e) {
                $transaction->rollBack();
            }
        }

        return true;
    }
}
