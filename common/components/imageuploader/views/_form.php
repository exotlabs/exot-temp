<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

?>

<div class="card">
    <div class="card-body">
        <?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i>Feltöltve!</h4>
            <?= Yii::$app->session->getFlash('success') ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->hasFlash('error')): ?>
            <div class="alert alert-error alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i>Hiba történt!</h4>
            <?= Yii::$app->session->getFlash('error') ?>
            </div>
        <?php endif; ?>
        <?php
        $form = ActiveForm::begin([
            'options'=>[
                'enctype'=>'multipart/form-data'
            ]
        ]);

        echo $form->field($filesUpload, 'files[]')->label(false)->widget(FileInput::classname(), [
            'options'=>$options,
            'pluginOptions' => $pluginOptions
        ]
        );
        
        echo Html::submitButton('Feltöltés',
            [
                'class'=> 'btn btn-primary'
            ]
        );
        ActiveForm::end();
        ?>

        
    </div>
</div>