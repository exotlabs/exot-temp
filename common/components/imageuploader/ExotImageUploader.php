<?php

namespace common\components\imageuploader;

use Yii;
use yii\helpers\Html;
use yii\base\Widget;
use common\components\imageuploader\models\FilesUploadForm;
use yii\web\UploadedFile;
/**
 *
 */
class ExotImageUploader extends Widget
{
    public $pluginOptions;
    public $options;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $filesUpload = new FilesUploadForm;
        /* if upload images */
        if ($filesUpload->load(Yii::$app->request->post())) {
            /* get the uploaded file instance. */
            $filesUpload->uploadedFiles = UploadedFile::getInstances($filesUpload, 'files');   
            
            if(!empty($filesUpload->uploadedFiles)){
                if($filesUpload->upload()){
                    Yii::$app->session->setFlash('success', "Kép feltöltése sikerült");
                }else {
                    Yii::$app->session->setFlash('error', "Hiba történt a feltöltés során. ");
                }
            }
        }

        return $this->render('_form', [
            'filesUpload'=>$filesUpload,
            'pluginOptions' => $this->pluginOptions,
            'options' => $this->options,
        ]);
    }
}
 ?>
