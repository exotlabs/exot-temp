<?php

namespace common\components;


use yii\helpers\VarDumper;

class GeocodeComponent
{
    public $apiKey = 'NA';

    /**
     * Geocode address via google api.
     *
     * @param $q
     * @return array
     */
    public function getCoded($q)
    {

        $q = str_replace(" ", "%20", $q);
        // Gets the cityies by q param
        $url = "https://maps.googleapis.com/maps/api/place/autocomplete/json" .
            "?input=" . $q . "&sensor=false&types=(regions)&key=" . $this->apiKey . "&components=country:HUN|country:hu|country:hu";
        $data = file_get_contents($url);
        $dataArray = json_decode($data, true);
        $resultArray = [];
        if (count($dataArray["predictions"]) > 0) {
            foreach ($dataArray["predictions"] as $currentCity) {
                $name = $currentCity["structured_formatting"]["main_text"];
                // Get latitude and longitude params
                $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" .
                    str_replace(" ", "%20", $name) . "&sensor=false&key=" . $this->apiKey;
                $data = file_get_contents($url);
                $dataArray = json_decode($data, true);
                $lat = "";
                $lng = "";
                if (isset($dataArray["results"][0]) && isset($dataArray["results"][0]["geometry"]) && isset($dataArray["results"][0]["geometry"]["location"])) {
                    $lat = $dataArray["results"][0]["geometry"]["location"]["lat"];
                    $lng = $dataArray["results"][0]["geometry"]["location"]["lng"];
                }
                $resultArray[] = ["name" => $name, "lat" => $lat, "lng" => $lng];
            }
        }

        // Gets the streets and others by q param
        $url = "https://maps.googleapis.com/maps/api/place/autocomplete/json" .
            "?input=" . $q . "&sensor=false&types=address&key=" . $this->apiKey . "&components=country:HUN|country:hu|country:hu";
        $data = file_get_contents($url);
        $dataArray = json_decode($data, true);
        if (count($dataArray["predictions"]) > 0) {
            foreach ($dataArray["predictions"] as $currentAddress) {
                $name = $currentAddress["description"];
                // Get latitude and longitude params
                $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . str_replace(" ", "%20", $name) . "&sensor=false&key=" .  $this->apiKey;
                $data = file_get_contents($url);
                $dataArray = json_decode($data, true);
                $lat = "";
                $lng = "";
                if (isset($dataArray["results"][0]) && isset($dataArray["results"][0]["geometry"]) && isset($dataArray["results"][0]["geometry"]["location"])) {
                    $lat = $dataArray["results"][0]["geometry"]["location"]["lat"];
                    $lng = $dataArray["results"][0]["geometry"]["location"]["lng"];
                }
                $resultArray[] = ["name" => $name, "lat" => $lat, "lng" => $lng];
            }
        }

        return $resultArray;
    }

    /**
     * Gets first element of the response.
     * @param $q
     * @return array|bool|mixed
     */
    public function getCodedFirst($q) {
        $resp = $this->getCoded($q);
        $firstResp = false;
        \Yii::debug('resp:'.VarDumper::dumpAsString($resp));
        if (is_array($resp)) {
            if (!empty($resp[0])) {
                $firstResp = $resp[0];
            } else {
                $firstResp = $resp;
            }
        }
        return $firstResp;
    }
}