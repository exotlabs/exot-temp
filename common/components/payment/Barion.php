<?php

namespace common\components\payment;

use common\models\SubscriptionPayment;
use common\models\Subscription;
use Barion\common\BarionEnvironment;
use Yii;

class Barion
{
    private $config;

    //https://doksi.barion.com/PaymentStatus
    private $dictStatus = [
        'Prepared'  => SubscriptionPayment::STATUS_PREPARED,
        'Reserved'  => SubscriptionPayment::STATUS_PREPARED,
        'Succeeded' => SubscriptionPayment::STATUS_OK,
        'Canceled'  => SubscriptionPayment::STATUS_ERROR,
        'Failed'    => SubscriptionPayment::STATUS_ERROR,
        'Expired'    => SubscriptionPayment::STATUS_ERROR,
    ];

    /**
     * Barion constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        //Check config params content
        $this->config = $this->checkConfig();

        //set up a client
        $env = $this->config['env'] == 'prod' ? BarionEnvironment::Prod : BarionEnvironment::Test;
        $this->client = new \Barion\BarionClient($this->config['key'], $this->config['version'], $env);
    }

    /**
     * Create a barion payment object
     * @param int $id subscription payment id
     * @throws \Exception
     */
    public function pay($id)
    {
        //Check incoming params
        $subscriptionPayment = $this->checkParams($id);

        //subscription
        $subscription = $subscriptionPayment->getSubscription()->one();

        //recurrence
        $recurrence = [
            'init' => false,
            'id' => $subscription->recurrence,
        ];
        if (empty($recurrence['id'])) {
            //missing recurrence on current subscription
            $subscriptionInit = Subscription::find()
                ->andWhere(['user_id' => $subscription->user_id])
                ->andWhere(['target' => $subscription->target])
                ->andWhere(['not', ['recurrence' => null]])
                ->orderBy(['id' => SORT_DESC])
                ->one();
            if (empty($subscriptionInit->recurrence)) {
                //missing recurrence on all subscription
                $recurrence['init'] = true;
                $recurrence['id'] = $subscription->recurrence = $subscription->id."-".$subscription->user_id."-".substr(md5("barion-".$subscription->id), 0, 5);
                $subscription->save();
            } else {
                $recurrence['id'] = $subscriptionInit->recurrence;
            }
        }

        //subscription plan
        $subscriptionPlan = $subscription->getSubscriptionPlan()->one();

        //https://packagist.org/packages/pumuklisk8/barion-web-php
        //https://doksi.barion.com/Payment-Start-v2
        $item = new \Barion\models\ItemModel();
        $item->Name = "Éholtóság, {$subscriptionPlan->name}, ".date('Y. m.');
        $item->Description = "Lejárat: ".date('Y.m.d H:i', $subscription->end_at);
        $item->Quantity = 1;
        $item->Unit = "hónap";
        $item->UnitPrice = $subscriptionPayment->price;
        $item->ItemTotal = $subscriptionPayment->price;
        $item->SKU = "PLAN-".$subscriptionPlan->id;

        $trans = new \Barion\models\PaymentTransactionModel();
        $trans->POSTransactionId = "TRANS-".$subscriptionPayment->id;
        $trans->Payee = $this->config['shop'];
        $trans->Total = $subscriptionPayment->price;
        //$trans->Comment = "Test transaction containing the product";
        $trans->AddItem($item);

        $ppr = new \Barion\models\PreparePaymentRequestModel();
        $ppr->GuestCheckout = true;
        $ppr->PaymentType = \Barion\common\PaymentType::Immediate;
        $ppr->InitiateRecurrence = $recurrence['init'];
        $ppr->RecurrenceId = $recurrence['id'];

        $ppr->FundingSources = array(\Barion\common\FundingSourceType::All);
        $ppr->PaymentRequestId = "PAYMENT-".$subscriptionPayment->id;
        //$ppr->PayerHint = "user@example.com";
        $ppr->Locale = \Barion\common\UILocale::HU;
        $ppr->OrderNumber = "SUBSCRIPTION-".$subscription->id;
        //$ppr->ShippingAddress = "12345 NJ, Example ave. 6.";
        //$ppr->RedirectUrl = "http://webshop.example.com/afterpayment";
        //$ppr->CallbackUrl = "http://webshop.example.com/processpayment";
        $ppr->currency = 'HUF';
        $ppr->AddTransaction($trans);
        try {
            $myPayment = $this->client->PreparePayment($ppr);
        } catch (\Exception $e) {
            foreach ($e->getTrace() as $trace) {
                if (!empty($trace['args']) && !empty($trace['args'][0]) && !empty($trace['args'][0]['Errors'])) {
                    $error = $trace['args'][0]['Errors'][0];
                    $subscriptionPayment->response = $subscriptionPayment->response.json_encode($error)."\n------------------\n";
                    $subscriptionPayment->save();
                    if (in_array($error['ErrorCode'], ['InvalidRecurrenceId', 'OriginalPaymentWasntSuccessful'])) {
                        $subscription = Subscription::find()->where(['recurrence' => $recurrence['id']])->one();
                        $subscription->recurrence = null;
                        $subscription->save();
                        $this->pay($id);
                    }
                    throw new \Exception($error['Description']." ({$error['HappenedAt']})");
                }
            }
            $subscriptionPayment->response = $subscriptionPayment->response.json_encode($e)."\n------------------\n";
            $subscriptionPayment->save();
            throw $e;
        }
        $subscriptionPayment->response = $subscriptionPayment->response.json_encode($myPayment)."\n------------------\n";
        $subscriptionPayment->payment_id = $myPayment->PaymentId;
        $status = isset($this->dictStatus[$myPayment->Status])
            ? $this->dictStatus[$myPayment->Status]
            : SubscriptionPayment::STATUS_ERROR;
        $subscriptionPayment->status = $status;
        $subscriptionPayment->save();

        if ($myPayment->Status == 'Succeeded') {
            header("location: /payment/barion/return?paymentId={$myPayment->PaymentId}");
        } else {
            header("location: {$myPayment->PaymentRedirectUrl}");
        }
        die;
        
        // if(php_sapi_name() !== 'cli'){
        //     if ($myPayment->Status == 'Succeeded') {
        //         header("location: /payment/barion/return?paymentId={$myPayment->PaymentId}");
        //         die;
        //     } else {
        //         return header("location: {$myPayment->PaymentRedirectUrl}");
        //         die;
        //     }
        // }
        // return;
    }

    /**
     * Get barion payment status
     * @param int $id barion payment id
     * @return array payment details
     * @throws \Exception
     */
    public function status($id)
    {
        Yii::info("Barion returned: payment_id: $id", 'payment');

        $paymentDetails = $this->client->GetPaymentState($id);
        Yii::info($paymentDetails, 'payment');

        try {
            $paymentRequestId = preg_replace("#\D#", "", $paymentDetails->PaymentRequestId);
            $subscriptionPayment = SubscriptionPayment::findOne([
                'payment_id' => $paymentDetails->PaymentId,
            ]);

            if(empty($subscriptionPayment)){
                throw new \Exception('SubscriptionPayment empty. Payment id: '.$paymentDetails->PaymentId);
            }

            $subscriptionPayment->response = $subscriptionPayment->response.json_encode($paymentDetails)."\n------------------\n";
            $subscriptionPayment->save();

            if (empty($subscriptionPayment->id) || $subscriptionPayment->id != $paymentRequestId) {
                throw new \Exception('Unknown payment id: '.$id);
            }

            $status = isset($this->dictStatus[$paymentDetails->Status])
                ? $this->dictStatus[$paymentDetails->Status]
                : SubscriptionPayment::STATUS_ERROR;
            $subscriptionPayment->status = $status;
            $subscriptionPayment->paid_at = strtotime($paymentDetails->CompletedAt);
            $subscriptionPayment->save();
            Yii::info("Barion payment status: $status", 'payment');

            if($subscriptionPayment->status === SubscriptionPayment::STATUS_OK){
                $subscriptionPayment->setSuccessPaymentDetails();
            }else{
                $subscriptionPayment->setErrorPaymentDetails();
            }
    
            return [
               'paymentId' => $paymentDetails->PaymentId,
               'status' => $paymentDetails->Status,
               'paid_at' => $paymentDetails->CompletedAt,
               'total' => $paymentDetails->Total,
            ];
        } catch (\Exception $e) {
            $subscriptionPayment->status = SubscriptionPayment::STATUS_ERROR;
            foreach ($e->getTrace() as $trace) {
                if (is_array($trace) && !empty($trace['args']) && !empty($trace['args'][0]) && !empty($trace['args'][0]['Errors'])) {
                    $error = $trace['args'][0]['Errors'][0];
                    $subscriptionPayment->response = $subscriptionPayment->response.json_encode($error)."\n------------------\n";
                    $subscriptionPayment->save();
                    Yii::error($error['Description'], 'payment');
                    throw new \Exception($error['Description']." ({$error['HappenedAt']})");
                }
            }
            $subscriptionPayment->response = $subscriptionPayment->response.json_encode($e)."\n------------------\n";
            $subscriptionPayment->save();
            $subscriptionPayment->setErrorPaymentDetails();
            throw $e;
        }
    }

    /**
     * Check incoming params
     * @param int $id subscription payment id
     * @return object SubscriptionPayment|null
     * @throws \Exception
     */
    private function checkParams($id)
    {
        if (empty($id)) {
            throw new \Exception('Missing param: id');
        }

        //find subscription payment
        $subscriptionPayment = SubscriptionPayment::findOne(['id' => $id]);
        if (empty($subscriptionPayment->id)) {
            throw new \Exception('Unknown subscription payment: '.$id);
        }

        return $subscriptionPayment;
    }

    /**
     * Check config params content
     * @return array config|null
     * @throws \Exception
     */
    private function checkConfig()
    {
        //config check
        $config = Yii::$app->params['payment'];
        if (empty($config['barion'])) {
            throw new \Exception('General error, missing Barion config');
        }
        $config = $config['barion'];
        foreach (['env', 'shop', 'key', 'version'] as $param) {
            if (empty($config[$param])) {
                throw new \Exception('General error, missing Barion config: '.$param);
            }
        }
        return $config;
    }
}
