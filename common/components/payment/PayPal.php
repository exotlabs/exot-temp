<?php

namespace common\components\payment;

use Yii;
use common\models\SubscriptionPayment;
use common\models\PaypalAgreementDetails;
use common\models\Subscription;
use Barion\common\BarionEnvironment;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use PayPal\Api\Agreement;
use PayPal\Api\AgreementStateDescriptor;
use PayPal\Api\Payer;
use PayPal\Api\Plan;
use PayPal\Api\ShippingAddress;
use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;

class PayPal
{
    private $config;

    public $agreementStates = [
        'PENDING'  => SubscriptionPayment::PP_STATUS_PENDING,
        'ACTIVE'  => SubscriptionPayment::PP_STATUS_ACTIVE,
        'SUSPENDED' => SubscriptionPayment::PP_STATUS_SUSPENDED,
        'CANCELLED'  => SubscriptionPayment::PP_STATUS_CANCELLED,
        'EXPIRED'    => SubscriptionPayment::PP_STATUS_EXPIRED,
    ];

    /**
     * PayPal constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        //Check config params content
        $this->config = $this->checkConfig();

        //set up a client
        $this->apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential($this->config['clientID'], $this->config['secretID'])
        );
        $this->apiContext->setConfig(
            array(
                'log.LogEnabled' => true,
                'log.FileName' => Yii::getAlias('@runtime') . '/logs/paypal.log',
                'log.LogLevel' => 'DEBUG',
                'mode' => 'sandbox',
            )
        );
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function pay($id)
    {
        //Check incoming params
        $subscriptionPayment = $this->checkParams($id);

        //subscription
        $subscription = $subscriptionPayment->getSubscription()->one();

        //recurrence
        $recurrence = [
            'init' => false,
            'id' => $subscription->recurrence,
        ];
        if (empty($recurrence['id'])) {
            //missing recurrence on current subscription
            $subscriptionInit = Subscription::find()
                ->andWhere(['user_id' => $subscription->user_id])
                ->andWhere(['target' => $subscription->target])
                ->andWhere(['status' => Subscription::STATUS_ACTIVE])
                ->andWhere(['not', ['recurrence' => null]])
                ->orderBy(['id' => SORT_DESC])
                ->one();
            if (empty($subscriptionInit->recurrence)) {
                //missing recurrence on all subscription
                $recurrence['init'] = true;
                $recurrence['id'] = $subscription->recurrence = $subscription->id."-".$subscription->user_id."-".substr(md5("paypal-".$subscription->id), 0, 5);
                $subscription->save();
            } else {
                $recurrence['id'] = $subscriptionInit->recurrence;
            }
        }

        //subscription plan
        $subscriptionPlan = $subscription->getSubscriptionPlan()->one();

        $startDate = date('Y-m-d\TH:i:s\Z', strtotime('+1 day'));
        $agreement = new Agreement();
        $agreement->setName($subscriptionPlan->name)
            ->setDescription($subscriptionPlan->description)
            ->setStartDate($startDate);

        // Set plan id
        $definedPlan = $this->defineBillingPlan($subscriptionPlan, $subscriptionPayment);
        $planID = $this->createAndActivateBillingPlan($definedPlan, $subscriptionPayment);
        $plan = new Plan();
        $plan->setId($planID);
        $agreement->setPlan($plan);

        // Add payer type
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);

        try {
            // Create agreement
            $agreement = $agreement->create($this->apiContext);

            // Extract approval URL to redirect user
            $approvalUrl = $agreement->getApprovalLink();
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            $ex->getCode();
            $ex->getData();
            $subscriptionPayment->response = $subscriptionPayment->response.json_encode($ex)."\n------------------\n";
            $subscriptionPayment->save();
        } catch (Exception $ex) {
            $subscriptionPayment->response = $subscriptionPayment->response.json_encode($ex)."\n------------------\n";
            $subscriptionPayment->save();
        }

        $query_str = parse_url($approvalUrl, PHP_URL_QUERY);
        parse_str($query_str, $query_params);
        
        $subscriptionPayment->response = $subscriptionPayment->response.json_encode(json_decode($agreement))."\n------------------\n";
        $subscriptionPayment->pp_token = $query_params['token'];
        $subscriptionPayment->status = SubscriptionPayment::PP_STATUS_PREPARED;
        $subscriptionPayment->save();

        header("location: ".$approvalUrl);
        die();
    }

    public function defineBillingPlan($subscriptionPlan, $subscriptionPayment)
    {
        $plan = new Plan();
        $plan->setName($subscriptionPlan->name)
            ->setDescription($subscriptionPlan->description)
            ->setType('fixed');

        // Set billing plan definitions
        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition->setName('Regular Payments')
            ->setType('REGULAR')
            ->setFrequency($this->config['frequency'])
            ->setFrequencyInterval($this->config['frequencyInterval'])
            ->setCycles($this->config['cycles'])
            ->setAmount(
                new Currency(
                    array(
                        'value' => $subscriptionPayment->price,
                        'currency' => $this->config['currency']
                    )
                )
            );

        // Set merchant preferences
        $merchantPreferences = new MerchantPreferences();
        $merchantPreferences->setReturnUrl($this->config['successURL'])
            ->setCancelUrl($this->config['cancelURL'])
            ->setAutoBillAmount('yes')
            ->setInitialFailAmountAction($this->config['initialFailAmountAction'])
            ->setMaxFailAttempts($this->config['maxFailAttempts']);

        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);

        $subscriptionPayment->response = $subscriptionPayment->response.json_encode(json_decode($plan))."\n---------\n";
        $subscriptionPayment->save();

        return $plan;
    }

    public function createAndActivateBillingPlan($plan, $subscriptionPayment)
    {
        try {
            $createdPlan = $plan->create($this->apiContext);

            try {
                $patch = new Patch();
                $value = new PayPalModel('{"state":"ACTIVE"}');
                $patch->setOp('replace')
                ->setPath('/')
                ->setValue($value);
                $patchRequest = new PatchRequest();
                $patchRequest->addPatch($patch);
                $plan->update($patchRequest, $this->apiContext);
                $createdPlan = Plan::get($plan->getId(), $this->apiContext);
            } catch (PayPal\Exception\PayPalConnectionException $ex) {
                $ex->getCode();
                $ex->getData();
                $subscriptionPayment->response = $subscriptionPayment->response.json_encode($ex)."\n----------------\n";
                $subscriptionPayment->save();
            } catch (Exception $ex) {
                $subscriptionPayment->response = $subscriptionPayment->response.json_encode($ex)."\n----------------\n";
                $subscriptionPayment->save();
            }
        } catch (Exception $ex) {
            $ex->getMessage();
            $ex->getCode();
            $subscriptionPayment->response = $subscriptionPayment->response.json_encode($ex)."\n----------------\n";
            $subscriptionPayment->save();
        }

        $subscriptionPayment->response = $subscriptionPayment->response.json_encode(json_decode($createdPlan))."\n---------------\n";
        $subscriptionPayment->save();

        return $createdPlan->getId();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getParams()
    {
        $this->checkConfig();
        $params = Yii::$app->params['payment']['paypal'];
        return $params;
    }

    /**
     * Check incoming params
     * @param int $id subscription payment id
     * @return object SubscriptionPayment|null
     * @throws \Exception
     */
    private function checkParams($id)
    {
        if (empty($id)) {
            throw new \Exception('Missing param: id');
        }

        //find subscription payment
        $subscriptionPayment = SubscriptionPayment::findOne(['id' => $id]);

        if (empty($subscriptionPayment->id)) {
            throw new \Exception('Unknown subscription payment: '.$id);
        }

        return $subscriptionPayment;
    }

    /**
     * Check config params content
     * @return array config|null
     * @throws \Exception
     */
    private function checkConfig()
    {
        //config check
        $config = Yii::$app->params['payment'];
        if (empty($config['paypal'])) {
            throw new \Exception('General error, missing PayPal config');
        }
        $config = $config['paypal'];
        foreach (['clientID', 'secretID', 'successURL', 'cancelURL', 'initialFailAmountAction', 'maxFailAttempts', 'frequencyInterval', 'frequency', 'cycles', 'currency'] as $param) {
            if (!isset($config[$param])) {
                throw new \Exception('General error, missing PayPal config: '.$param);
            }
        }
        return $config;
    }

    public function executeAgreement(string $token)
    {
        try {
            // Execute agreement
            $agreement = new Agreement();
            $agreement->execute($token, $this->apiContext);
            
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            $ex->getCode();
            $ex->getData();
            die($ex);
        } catch (Exception $ex) {
            die($ex);
        }

        $subscriptionPayment = SubscriptionPayment::findOne(['pp_token' => $token]);

        if(!empty($agreement)){
            $paypalAgreementDetails = new PaypalAgreementDetails();
            $paypalAgreementDetails->updateAggreement($agreement, $subscriptionPayment->subscription->id);            
        }else{
            Yii::error("Agreement is null", 'payment');
        }

        $subscriptionPayment->response = $subscriptionPayment->response.json_encode(json_decode($agreement))."\n---------------\n";
        $subscriptionPayment->payment_id = $agreement->getId();
        $subscriptionPayment->cycle = $paypalAgreementDetails->cycles_completed;
        $subscriptionPayment->status = SubscriptionPayment::PP_STATUS_ACTIVE;
        $subscriptionPayment->save();

        return $agreement;
    }

    public function getAgreement($agreementId)
    {
        $agreement = new Agreement();
        $agreement->get($agreementId, $this->apiContext);

        return $agreement;
    }

    public function updateAgreements()
    {
        $subscriptionPayments = (new \yii\db\Query())
            ->select(['id', 'subscription_id', 'user_id', 'price', 'paid_at', 'payment_id', 'status'])
            ->from('subscription_payment')
            ->where(['payment_type' => 1])->andWhere(['not', ['payment_id' => null]])
            ->all();

        foreach ($subscriptionPayments as $payment) {
            $payment = SubscriptionPayment::findOne(['id' => $payment['id']]);
            $agreement = new Agreement();
            $state = $agreement->get($payment['payment_id'], $this->apiContext)->getState();
            if ($state == 'Cancelled') {
                $payment['status'] = SubscriptionPayment::PP_STATUS_CANCELLED;
                $payment['isCancel'] = 1;
                $payment->save();
            } elseif ($state == 'Expired') {
                $payment['status'] = SubscriptionPayment::PP_STATUS_EXPIRED;
                $payment->save();
            }
        }

        print_r("updated");
    }
}
