<?php

namespace common\components;

use common\models\PartnerMonthlyStat;
use common\models\User;
use Yii;
use yii\helpers\Url;

class EmailComponent {

    /**
     * Send email to confirm e-mail address
     * @param User $user
     * @param String $link
     * @return bool
     */
    public function sendConfirmEmail(User $user, $link)
    {
        return Yii::$app->mailer->compose(['html' => 'confirm-html', 'text' => 'confirm-text'], ['user' => $user, 'link'=> $link])
                        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ''])
                        ->setTo($user->email)
                        ->setSubject('Erősítse meg az email címét')
                        ->send();
    }

    /**
     * Reset password token mail sending.
     * @param User $user
     * @param string $resetLink
     * @return bool
     */
    public function sendResetPasswordTokenEmail(User $user, $resetLink)
    {
        return Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                            ['user' => $user, 'resetLink' => $resetLink]
                        )
                        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ''])
                        ->setTo($user->email)
                        ->setSubject('Jelszó módosítás - ' . Yii::$app->name)
                        ->send();
    }


    /**
     * Send e-mail confirm changing e-mail address
     * @param User $user
     * @return bool
     */
    public function sendConfirmChangeEmail(User $user)
    {
        $link = Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/confirmchange', 'token' => $user->confirm_token]);
        return Yii::$app->mailer->compose(['html' => 'confirmChangeEmail-html', 'text' => 'confirmChangeEmail-text'], ['user' => $user, 'link'=> $link])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ''])
            ->setTo($user->email_new)
            ->setSubject('Erősítse meg az email cím módosítását')
            ->send();
    }

    /**
     * Sends last e-mail to deleting user.
     * @param User $user
     * @return bool
     */
    public function sendDeleteUserEmail(User $user)
    {
        return Yii::$app->mailer->compose(['html' => 'deleteUserEmail-html', 'text' => 'deleteUserEmail-text'], ['user' => $user])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ''])
            ->setTo($user->email)
            ->setSubject('Profil törlésének igazolása')
            ->send();
    }

    /**
     * Sends last e-mail to cancel subscription event.
     * @param User $user
     * @return bool
     */
    public function sendCancelSubscriptionEmail(User $user)
    {
        return Yii::$app->mailer->compose(['html' => 'cancelSubscription-html', 'text' => 'cancelSubscription-text'], ['user' => $user])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ''])
            ->setTo($user->email)
            ->setSubject('Előfizetés leállítása')
            ->send();
    }

    public function sendCreatePartnerEmail(User $user, $link)
    {
        return Yii::$app->mailer->compose(['html' => 'createPartnerEmail-html', 'text' => 'createPartnerEmail-text'],
            ['user' => $user, 'link' => $link])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ''])
            ->setTo($user->email)
            ->setSubject('Eholtosag.hu Partneri belépés')
            ->send();
    }


    public function sendGeneratedMonthlyPdf(string $email, string $month, string $pdf, string $commission, bool $accountingRelationship)
    {
        return Yii::$app->mailer->compose(['html' => 'monthlyPdf-html', 'text' => 'monthlyPdf-text'],
            ['month' => $month, 'commission' => $commission, 'accountingRelationship' => $accountingRelationship])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ''])
            ->setTo($email)
            ->setSubject('Eholtosag.hu Elszámolás')
            ->attach($pdf)
            ->send();        
    }


    public function sendPaidPdfEmail(string $pdf, PartnerMonthlyStat $stat)
    {
        return Yii::$app->mailer->compose(['html' => 'paidPdf-html', 'text' => 'paidPdf-text'],
            ["stat" => $stat])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ''])
            ->setTo('toma.andras@exot.hu'/*Yii::$app->params['supportEmail']*/)
            ->setSubject('Eholtosag.hu befizetés - '.$stat->getFormattedActualDate())
            ->attach($pdf)
            ->send();        
    }
}
