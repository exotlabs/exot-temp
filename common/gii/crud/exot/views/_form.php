<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card-body">

    <div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

        <?= "<?php " ?>$form = ActiveForm::begin([
            'fieldConfig' => ['options' => ['class' => 'form-group row']],
            'options' => [
                'class' => (isset($successSave)&&$successSave===true?"save-success":"")
            ]
        ]); ?>

        <div class="row">
            <div class="col-md-9">

                <div class="panel card panel-default">
                    <div class="panel-body card-body">
                        <?php
                        foreach ($generator->getColumnNames() as $attribute) {
                            if (in_array($attribute, $safeAttributes)) {
                                ?>
                                 <?="<?="?> $form->field(
                                    $model,
                                    '<?=$attribute?>',
                                    [
                                        'template' => '
                                            {label}
                                            <div class="col-lg-10">
                                                {input}
                                                <div class="help-block"></div>
                                            </div>
                                        ',
                                    ]
                                )->textInput(['maxlength' => true])->label($model->getAttributeLabel('<?=$attribute?>'), ['class'=>'col-form-label col-lg-2']) <?="?>\n"?>
                            <?php
                            }
                        }?>
                    </div>
                </div>

            </div>

            <div class="col-md-3">

                <div class="panel card panel-default">
                    <?="<?php"?> if (!$model->isNewRecord): <?="?>"?>
                    <ul class="list-group list-group-flush border-bottom">
                        <li class="list-group-item">
                            <span class="font-weight-semibold">Létrehozva:</span>
                            <div class="ml-auto"><?="<?="?> date('Y-m-d H:i:s', $model->created_at) <?="?>"?></div>
                        </li>
                        <li class="list-group-item">
                            <span class="font-weight-semibold">Módosítva:</span>
                            <div class="ml-auto"><?="<?="?> date('Y-m-d H:i:s', $model->updated_at) <?="?>"?></div>
                        </li>
                    </ul>
                    <?="<?php endif; ?>"?>

                    <div class="panel-body card-body">
                        <div class="record-info">
                            <div class="form-group d-flex">
                                <?="<?php"?> if ($model->isNewRecord): <?="?>"?>
                                <?= Html::submitButton(Yii::t('frontend', 'Mentés'), ['class' => 'btn btn-primary']) ?>
                                <?= Html::a(Yii::t('frontend', 'Mégsem'), ['adminindex'], ['class' => 'btn btn-default']) ?>
                                <?="<?php else: ?>"?>

                                <?="<?="?> Html::submitButton('<b><i class="icon-checkmark4"></i></b>' . Yii::t('frontend', 'Mentés'), ['class' => 'btn bg-teal-400 btn-labeled btn-labeled-left mr-auto']) <?="?>"?>

                                <?="<?="?> Html::a('<i class="icon-cog3 mr-2"></i>' . Yii::t('frontend', 'Törlés'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-outline-danger',
                                'data' => [
                                'confirm' => Yii::t('frontend', 'Biztosan törli az elemet?'),
                                'method' => 'post',
                                ],
                                ]) <?="?>"?>
                                <?="<?php endif; ?>"?>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="panel card panel-default">
                    <div class="panel-body card-body">
                    </div>
                </div>

            </div>

        <?= "<?php " ?>ActiveForm::end(); ?>

    </div>

<?="<?php \n"?>
$js = <<<JS
    $(function () {
        if ($("form").length > 0) {
            if ($("form").hasClass("save-success")) {
                new PNotify({
                    title: 'Mentés sikers',
                    text: 'A megadott értékek mentésre kerültek!',
                    addclass: 'bg-success border-success'
                });
            }
        }
    });
JS;
$this->registerJs($js, yii\web\View::POS_READY);
<?="?>"?>

</div>
