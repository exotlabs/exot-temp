<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;

echo $this->render("/admin/elements/_page_header.php", [
    "buttons" => [
        [
            "label" => "New <?= Inflector::camel2words(StringHelper::basename($generator->modelClass))?>",
            "class" => "icon-add",
            "link" => "/admin/item/create"
        ]
    ]
]);
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index content">

    <div class="card">

<?= $generator->enablePjax ? "    <?php Pjax::begin(['id' => 'index-list', 'timeout' => 5000]); ?>\n" : "" ?>

        <div class="datatable-header">
            <?= "<?php"?> $form = ActiveForm::begin([
                'method'=>'get',
                'fieldConfig' => [
                    'options' => [
                        'tag' => false,
                    ],
                ],
            ]); <?= "?>" ?>
            <div id="DataTables_Table_1_filter" class="dataTables_filter">
                <label><span>Keresés:</span>
                    <?= "<?="?> $form->field($searchModel, 'search')
                        ->textInput(['placeholder'=>'keresés az adatokban ...','type'=>'search','class' => '', 'aria-controls'=>'DataTables_Table_1'])
                        ->label(false);<?= "?>"?>
                </label>
            </div>
            <?= "<?php"?> ActiveForm::end(); <?="?>"?>
        </div>

<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>\common\components\ExotGridView::widget([
        'dataProvider' => $dataProvider,
        <?= !empty($generator->searchModelClass) ? "//'filterModel' => \$searchModel,     // Remove this if you want search input fields\n  'options' => [\n 'class' => ''\n    ],\n        'columns' => [\n" : "   'options' => [\n 'class' => 'card'\n    ],\n    'columns' => [\n"; ?>
            ['class' => 'yii\grid\SerialColumn'],

<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        if (++$count < 6) {
            //echo "            '" . $name . "',\n";
            echo "[\n   'attribute' => '".$name."',\n   'headerOptions' => ['class' => 'sort'],\n 'enableSorting' => true,\n 'filter' => false,\n],";
        } else {
            echo "            //'" . $name . "',\n";
        }
    }
} else {
    foreach ($tableSchema->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        if (++$count < 6) {
            //echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
            echo "[\n   'attribute' => '".$column->name."',\n   'headerOptions' => ['class' => 'sort'],\n 'enableSorting' => true,\n 'filter' => false,\n],";
        } else {
            echo "            //'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        }
    }
}
?>

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'footerRowOptions' => [
            'class' => 'datatable-footer'
        ],
        'pager' => [
            'class' => '\yii\widgets\LinkPager',
            'disabledListItemSubTagOptions' => [
                'tag' => 'a',
                'class' => 'paginate-button'
            ],
            'activePageCssClass' => 'current',
            'maxButtonCount' => 5,
            'prevPageLabel' => '←',
            'prevPageCssClass' => 'previous',
            'nextPageLabel' => '→',
            'nextPageCssClass' => 'next',
            'linkContainerOptions' => [
                'tag' => 'li',
                'class' => 'paginate_button',
            ],
            'linkOptions' => [
                'class' => 'text-dark'
            ],
            'pageCssClass' => '',
            'options' => [
                'tag' => 'div',
                'class' => 'dataTables_paginate paging_simple_numbers'
            ]
        ]
    ]); ?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>

        <!-- Modal -->
        <div class="modal fade" id="list-modal" tabindex="-1" role="dialog" aria-labelledby="list-modal-label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Biztosan törli a kiválasztott elemet?</h5>
                    </div>
                    <div class="modal-body">
                        Törlés megerősítésével véglegesen törli az elemet.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Mégsem</button>
                        <button type="button" class="btn btn-primary" data-url="/" id="delete-btn" onclick="window.location.href=$(this).data('url')">Törlés</button>
                    </div>
                </div>
            </div>
        </div>

<?= $generator->enablePjax ? "    <?php Pjax::end(); ?>\n" : '' ?>

    </div>

</div>
