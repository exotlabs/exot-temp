<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'inviteEmailFrom' => 'invite@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'uploadFolder' => '/uploads/',
    'nopicPath' => '/img/no_image_480x480.jpg',
    'domain' => 'http://system.exot.hu',
];
