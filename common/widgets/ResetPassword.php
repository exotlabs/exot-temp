<?php
namespace common\widgets;

use Yii;
use frontend\models\PasswordResetRequestForm;

class ResetPassword extends \yii\bootstrap\Widget
{
    public function run()
    {
        $model = new PasswordResetRequestForm();
        $response = [];
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                $response['success'] = true;
                $response['message'] = 'Check your email for further instructions.';
            } else {
                $response['success'] = false;
                $response['message'] = 'Sorry, we are unable to reset password for the provided email address.';
            }
        }

        return $this->render('resetPassword', [
            'model' => $model,
            'response' => $response
        ]);
    }
}