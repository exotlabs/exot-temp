<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
?>

<!-- Content area -->
<div class="content d-flex justify-content-center align-items-center">

    <!-- Password recovery form -->
    <?php Pjax::begin(['id' => 'recover-form']) ?>
    <?php $form = ActiveForm::begin([
        'id' => 'recovery-form',
        'options' => [
            'class' => 'login-form',
            'data-pjax' => true
        ],
        'fieldConfig' => [
            'errorOptions' => ['encode' => false],
        ],
    ]); ?>
        <div class="card mb-0">
            <div class="card-body">
                <div class="text-center mb-3">
                    <!--<i class="icon-spinner11 icon-2x text-warning border-warning border-3 rounded-round p-3 mb-3 mt-1"></i>-->
                    <span>
                        <img src="/img/eholtosag_icon_1024x1024.png" style="width:90px;height:90px;padding-bottom:10px;">
                    </span>
                    <h5 class="mb-0">Jelszó helyreállítás</h5>
                    <span class="d-block text-muted">E-mailben küldjük a teendőket</span>
                </div>
                <div class="reset-message">
                    <?php
                    if (!empty($response)) {
                        if ($response["success"] === true) {
                            echo '<div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible"><button type="button" class="close" data-dismiss="alert"><span>×</span></button><span class="font-weight-semibold">Well done!</span>'.$response["message"].'</div>';
                        } else {
                            echo '<div class="alert alert-danger alert-styled-left alert-arrow-left alert-dismissible"><button type="button" class="close" data-dismiss="alert"><span>×</span></button><span class="font-weight-semibold">Well done!</span>'.$response["message"].'</div>';
                        }
                    }
                    ?>
                </div>
                <?= $form->field($model, 'email', [
                  'template' => "{error}\n<div class=\"form-group form-group-feedback form-group-feedback-right\">{input}<div class=\"form-control-feedback\">
                      <i class=\"icon-mail5 text-muted\"></i>
                  </div></div>"
                ])->textInput(['autofocus' => true, 'placeholder' => 'email'])->label(false) ?>
                <button type="submit" class="btn bg-blue btn-block"><i class="icon-spinner11 mr-2"></i> Új jelszó</button>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
    <!-- /password recovery form -->

</div>
<!-- /content area -->

<?php
$this->registerJs(
        "$(document).on('pjax:send', function (el, ev) {
                if ('relatedTarget' in el) {
                    $(el.relatedTarget).find('button').addClass('btn-loading disabled');
                    $(el.relatedTarget).find('button').find('i').attr('class', 'icon-spinner2 spinner mr-2');                
                }
            });
            ",
    yii\web\View::POS_END
);
?>