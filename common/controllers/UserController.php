<?php

namespace common\controllers;

use common\models\User;
use yii\web\Controller;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{

    /**
     * Return all user for select
     *
     * @return []
     */
    public static function getUserList()
    {
        $users = User::find()->all();
        $results = [];
        foreach ($users as $user) {
            $results[$user->id] = $user->getFullName();
        }
        return $results;
    }

}
