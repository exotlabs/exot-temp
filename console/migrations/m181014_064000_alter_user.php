<?php

use yii\db\Migration;

/**
 * Class m181014_064000_alterUserAddConfirmToken
 */
class m181014_064000_alter_user extends Migration {

    public function safeUp()
    {
        $this->addColumn('user', 'confirm_token', $this->string(255)->null());
        $this->addColumn('user', 'first_name', $this->string(255)->null());
        $this->addColumn('user', 'family_name', $this->string(255)->null());
        $this->addColumn('user', 'gender', $this->smallInteger()->null());
        $this->addColumn('user', 'age', $this->integer()->null());
        $this->addColumn('user', 'zip', $this->integer()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'confirm_token');
        $this->dropColumn('user', 'first_name');
        $this->dropColumn('user', 'family_name');
        $this->dropColumn('user', 'gender');
        $this->dropColumn('user', 'age');
        $this->dropColumn('user', 'zip');
    }

}
