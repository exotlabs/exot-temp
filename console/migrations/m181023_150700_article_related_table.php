<?php

use yii\db\Migration;

/**
 * Class m181023_150700_article_relation_table
 */
class m181023_150700_article_related_table extends Migration
{

    const ARTICLE_TABLE = '{{%article}}';
    const ARTICLE_RELATED_TABLE = '{{%article_related}}';
    const USER_TABLE = '{{%user}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::ARTICLE_RELATED_TABLE, [
            'id' => $this->primaryKey(),
            'article_id' => $this->integer()->notNull(),
            'related_id' => $this->integer()->notNull(),
            'created_user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_article_related_article_id', self::ARTICLE_RELATED_TABLE, 'article_id');
        $this->createIndex('idx_article_related_related_id', self::ARTICLE_RELATED_TABLE, 'related_id');
        $this->createIndex('idx_article_related_created_user_id', self::ARTICLE_RELATED_TABLE, 'created_user_id');

        $this->addForeignKey('fk_article_related_article_id', self::ARTICLE_RELATED_TABLE, 'article_id', self::ARTICLE_TABLE, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_article_related_related_id', self::ARTICLE_RELATED_TABLE, 'related_id', self::ARTICLE_TABLE, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_article_related_user_id', self::ARTICLE_RELATED_TABLE, 'created_user_id', self::USER_TABLE, 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::ARTICLE_RELATED_TABLE);
    }
}
