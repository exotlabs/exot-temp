<?php

use yii\db\Migration;

/**
 * Class m181019_132310_article_table
 */
class m181019_132310_article_table extends Migration
{

    const ARTICLE_TABLE = '{{%article}}';
    const USER_TABLE = '{{%user}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::ARTICLE_TABLE, [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'lead' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'status' => $this->tinyInteger(1),
            'published_user_id' => $this->integer()->notNull(),
            'published_at' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'type' => $this->tinyInteger(1),
        ], $tableOptions);

        $this->createIndex('idx_article_published_user_id', self::ARTICLE_TABLE, 'published_user_id');
        $this->createIndex('idx_article_published_at', self::ARTICLE_TABLE, 'published_at');
        $this->createIndex('idx_article_created_at', self::ARTICLE_TABLE, 'created_at');
        $this->createIndex('idx_article_updated_at', self::ARTICLE_TABLE, 'updated_at');

        $this->addForeignKey('fk_article_published_user_id', self::ARTICLE_TABLE, 'published_user_id', self::USER_TABLE, 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181019_132310_article_table cannot be reverted.\n";
        $this->dropTable(self::ARTICLE_TABLE);

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181019_132310_article_table cannot be reverted.\n";

        return false;
    }
    */
}
