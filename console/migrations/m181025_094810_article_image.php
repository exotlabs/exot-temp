<?php

use yii\db\Migration;

/**
 * Class m181025_094810_article_image
 */
class m181025_094810_article_image extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('article', 'image_id', $this->integer()->null());
        $this->createIndex('idx_article_image_id', 'article', 'image_id');
        $this->addForeignKey('fk_article_image_id', 'article', 'image_id', 'image', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181025_094810_article_image cannot be reverted.\n";
        $this->dropColumn('article', 'image_id');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181025_094810_article_image cannot be reverted.\n";

        return false;
    }
    */
}
