<?php

use yii\db\Migration;

/**
 * Class m181022_191044_add_image
 */
class m181022_191044_add_image extends Migration
{
    const TABLE_NAME = '{{%image}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'file_name' => $this->string(255)->notNull(),
            'width' => $this->integer()->notNull(),
            'height' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);

        $this->createIndex('idx_image_created_by', self::TABLE_NAME, 'created_by');
        $this->createIndex('idx_image_file_name', self::TABLE_NAME, 'file_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181022_191044_add_image cannot be reverted.\n";

        return false;
    }
    */
}
