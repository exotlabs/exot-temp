<?php

use yii\db\Migration;

/**
 * Class m180922_160639_init_rbac
 */
class m180922_160639_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        // create roles
        $user = $auth->createRole('user');
        $admin = $auth->createRole('admin');
     
        $auth->add($user);
        $auth->add($admin);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180922_160639_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
