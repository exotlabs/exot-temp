<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use common\widgets\ResetPassword;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo ResetPassword::widget(); ?>
