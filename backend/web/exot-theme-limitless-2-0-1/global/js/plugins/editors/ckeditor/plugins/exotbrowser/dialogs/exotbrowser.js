CKEDITOR.dialog.add( 'exotbrowserDialog', function( editor ) {
    return {
        title: 'Tartalom kiválasztása',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'tab-basic',
                label: 'Basic Settings',
                elements: [
                    {
                        type: 'select',
                        id: 'type',
                        label: 'Típus',
						default: 'Cikk',
                        items: [ [ 'Partner' ], [ 'Cikk' ], ['Adományozási oldal'] ],
                        validate: CKEDITOR.dialog.validate.notEmpty( "Nem lehet üres a típus" ),
                        onLoad : function () {
                            this.getInputElement().addClass('type-selector');
                        },
                        onChange: function () {
                            initSelect();
                        }
                    },
                    {
                        type: 'text',
                        id: 'content',
                        label: 'Tartalom',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Nem lehet üres a tartalom" ),
                        onLoad : function () {
                            this.getInputElement().addClass('for-autocomplete');
							initSelect();
                        }
                    },
                    {
                        type: 'text',
                        id: 'content_id',
                        inputStyle: 'display:none',
                        style: 'display:none',
                        onLoad : function () {
                            this.getInputElement().addClass('content-id-value');
                        },

                    }
                ]
            },
        ],
        onOk: function() {
            var dialog = this,
                selection = editor.getSelection(),
                element = selection.getStartElement();

            var exotbrowser = editor.document.createElement( 'a' );

            exotbrowser.setAttribute( 'title', dialog.getValueOf( 'tab-basic', 'content' ) );

            if (dialog.getValueOf( 'tab-basic', 'type' ) === 'Adományozási oldal') {
                //exotbrowser.setAttribute( 'href', '/' + dialog.getValueOf( 'tab-basic', 'content_id' ) );
                exotbrowser.setAttribute( 'href', '?adomanyozas' );
            } else if (dialog.getValueOf( 'tab-basic', 'type' ) === 'Partner') {
                exotbrowser.setAttribute( 'href', '?partner=' + dialog.getValueOf( 'tab-basic', 'content_id' ) );
            } else {
                exotbrowser.setAttribute( 'href', '?cikk=' + dialog.getValueOf( 'tab-basic', 'content_id' ) );
            }

            if (selection.getSelectedText() !== "") {
                exotbrowser.setText( selection.getSelectedText() );
                selection.remove;
            } else {
                exotbrowser.setText( dialog.getValueOf( 'tab-basic', 'content' ) );
            }

            editor.insertElement( exotbrowser );

        }
    };
});

/**
 * The autocomplete field init and it is handle the special page
 */
function initSelect() {
    if ($(".type-selector").val() === "Adományozási oldal") {
		$(".for-autocomplete").attr("disabled", "disabled");
        if ($('input.for-autocomplete').hasClass("ui-autocomplete-input")) {
            $('input.for-autocomplete').autocomplete("destroy");
        }
        $("input.content-id-value").val("adomanyozasi-oldal");
        $("input.for-autocomplete").val("Adományozási oldal");
    } else {
		$(".for-autocomplete").removeAttr("disabled");
        $("input.content-id-value").val("");
        $("input.for-autocomplete").val("");
        $("input.for-autocomplete").autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "/searchresult",
                    dataType: "json",
                    data: {
                        type: $(".type-selector").val(),
                        text: $(".for-autocomplete").val()
                    },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.name,
                                id: item.id,
                                value: item.name
                            };
                        }));
                    }
                });
            },
            select: function (event, ui) {
                $("input.content-id-value").val(ui.item.id);
                $(this).val(ui.item.label);
                return false;
            },
            minLength: 3,
        });
    }
}