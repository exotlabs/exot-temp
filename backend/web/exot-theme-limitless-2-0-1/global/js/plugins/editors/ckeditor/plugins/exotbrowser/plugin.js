CKEDITOR.plugins.add( 'exotbrowser', {
    icons: 'exotbrowser',
    init: function( editor ) {
        editor.addCommand( 'exotbrowser', new CKEDITOR.dialogCommand( 'exotbrowserDialog' ) );
        editor.ui.addButton( 'ExotBrowser', {
            label: 'Tartalom beszúrása',
            command: 'exotbrowser',
            toolbar: 'insert'
        });

        CKEDITOR.dialog.add( 'exotbrowserDialog', this.path + 'dialogs/exotbrowser.js' );
    }
});