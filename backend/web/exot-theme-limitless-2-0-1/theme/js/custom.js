/* ------------------------------------------------------------------------------
 *
 *  # Custom JS code
 *
 *  Place here all your custom js. Make sure it's loaded after app.js
 *
 * ---------------------------------------------------------------------------- */

$(function () {

    if (!$().alpaca) {
        console.warn('Warning - alpaca.min.js is not loaded.');
        return;
    } else {
        console.log('Alpaca loaded')
    }

    if (!$().uniform) {
        console.warn('Warning - uniform.min.js is not loaded.');
        return;
    } else {
        console.log('Uniform loaded')

        $("input.limitless-checkbox:checkbox").uniform();

        $("input.limitless-radio:radio").uniform();

    }

    if (!$().select2) {
        console.warn('Warning - select2.min.js is not loaded.');
        return;
    } else {
        console.log('Select2 loaded')

        $("select.limitless-select").select2({
            width: '100%'
        });

        $(document).on('pjax:end', function(data, status, xhr, options) {
            $("select.limitless-select").select2({
                width: '100%'
            });
        });
    }

    /* Navigator active select in sub page*/
    $(".nav.nav-sidebar a").each(function (idx) {
        var $aItem = $(this),
            url = window.location.href;

        if ($aItem.attr("href").length > 1 && url.indexOf($aItem.attr("href")) !== -1) {
            if (url.substr(-5) !== "admin") {
                $(".nav.nav-sidebar a:first").removeClass("selected");
            }
            $aItem.addClass("selected");
            if ($aItem.parent("li").parent("ul").hasClass("nav-group-sub")) {
                $aItem.parent("li").parent("ul").show();
                $aItem.parent("li").parent("ul").parent("li").addClass("nav-item-open");
            }
        }
    });

});