<?php
use frontend\widgets\RelatedArticlesWidget;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticleSearch */
/* @var $parentId integer */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $relatedDataProvider yii\data\ActiveDataProvider */
?>

<?php Pjax::begin(["id" => "related-list", 'timeout' => 5000,
    'formSelector' => '#relatedFilterForm']); ?>

<h4>Kapcsolódó cikkek szerkesztése</h4>
<?php if ($relatedDataProvider->totalCount>0) { ?>
    <?= \common\components\ExotGridView::widget([
        'dataProvider' => $relatedDataProvider,
        //'filterModel' => $searchModel,     // Remove this if you want search input fields
        'options' => [
            'class' => '',
        ],
        'columns' => [
            [
                'attribute' => 'title',
                'headerOptions' => ['class' => 'sort'],
                'enableSorting' => true,
                'filter' => false,
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->title;
                }
            ],[
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'type',
                'headerOptions' => ['class' => 'sort'],
                'enableSorting' => true,
                'filter' => false,
                'value' => function ($model) {
                    return $model->getTypeLabel();
                }
            ],[
                'attribute' => 'created_at',
                'headerOptions' => ['class' => 'sort'],
                'enableSorting' => true,
                'filter' => false,
                'value' => function ($model) {
                    return date("Y.m.d H:i", $model->created_at);
                }
            ],[
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'published_user_id',
                'headerOptions' => ['class' => 'sort'],
                'enableSorting' => true,
                'filter' => false,
                'value' => function ($model) {
                    return $model->publishedUser->getFullName();
                }
            ],[
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'status',
                'headerOptions' => ['class' => 'sort'],
                'enableSorting' => true,
                'filter' => false,
                'value' => function ($model) {
                    return $model->getStatusLabel();
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{remove}',
                'header' => 'Művelet',
                'buttons' =>
                    [
                        'remove'=>function($url,$model,$key) use ($parentId)
                        {
                            return Html::button( "Eltávolítás" ,['onclick' =>
                                "$.ajax({
                                    type: 'POST',
                                    url: '/admin/article/removerelated',
                                    data: {
                                        id:$model->id,
                                        parent_id:$parentId
                                    }, success: function(result) {
                                    }, error: function(result) {
                                        //console.log('server error');
                                    }
                                }).done(function() {
                                    $.pjax.reload({container: '#related-list', timeout: 5000});                                
                                });",
                                'class' =>'badge badge-danger'
                            ]);
                        },
                    ],
            ],
        ],
        'footerRowOptions' => [
            'class' => 'datatable-footer'
        ],
    ]); ?>
<?php } ?>
    <?php $form = ActiveForm::begin([
        'method'=>'post',
        'id' => 'relatedFilterForm',
        'fieldConfig' => [
            'options' => [
                'tag' => false,
            ],
        ],
    ]); ?>
    <div id="DataTables_Table_1_filter" class="dataTables_filter">
        <label><span>Keresés:</span>
            <?= $form->field($searchModel, 'search')
                ->textInput(['placeholder'=>'keresés az adatokban ...','type'=>'search','class' => '', 'aria-controls'=>'DataTables_Table_1'])
                ->label(false);?>                </label>
    </div>
    <?php ActiveForm::end(); ?>

<?= \common\components\ExotGridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,     // Remove this if you want search input fields
    'options' => [
        'class' => ''
    ],
    'columns' => [
        [
            'attribute' => 'title',
            'headerOptions' => ['class' => 'sort'],
            'enableSorting' => true,
            'filter' => false,
            'format' => 'raw',
            'value' => function ($model) {
                return $model->title;
            }
        ],[
            'class' => 'yii\grid\DataColumn',
            'attribute' => 'type',
            'headerOptions' => ['class' => 'sort'],
            'enableSorting' => true,
            'filter' => false,
            'value' => function ($model) {
                return $model->getTypeLabel();
            }
        ],[
            'attribute' => 'created_at',
            'headerOptions' => ['class' => 'sort'],
            'enableSorting' => true,
            'filter' => false,
            'value' => function ($model) {
                return date("Y.m.d H:i", $model->created_at);
            }
        ],[
            'class' => 'yii\grid\DataColumn',
            'attribute' => 'published_user_id',
            'headerOptions' => ['class' => 'sort'],
            'enableSorting' => true,
            'filter' => false,
            'value' => function ($model) {
                return $model->publishedUser->getFullName();
            }
        ],[
            'class' => 'yii\grid\DataColumn',
            'attribute' => 'status',
            'headerOptions' => ['class' => 'sort'],
            'enableSorting' => true,
            'filter' => false,
            'value' => function ($model) {
                return $model->getStatusLabel();
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{add}',
            'header' => 'Művelet',
            'buttons' =>
                [
                    'add'=>function($url,$model,$key) use ($parentId)
                    {
                        return Html::button( "Hozzáadás" ,['onclick' =>
                        "$.ajax({
                                type: 'POST',
                                url: '/admin/article/addrelated',
                                data: {
                                    id:$model->id,
                                    parent_id:$parentId
                                }, success: function(result) {
                                }, error: function(result) {
                                    //console.log('server error');
                                }
                            }).done(function() {
                                    $.pjax.reload({container: '#related-list', timeout: 5000});                                
                            });"
                        ,'class' =>'badge badge-success']);
                    },
                ],
        ],
    ],
    'footerRowOptions' => [
        'class' => 'datatable-footer'
    ],
]); ?>
<?php Pjax::end(); ?>