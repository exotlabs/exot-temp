<div class="form-group alpaca-field alpaca-field-file alpaca-optional alpaca-autocomplete alpaca-create alpaca-top"
     data-alpaca-field-id="file-styled" data-alpaca-field-path="/" data-alpaca-field-name="">
    <label class="control-label alpaca-control-label" for="file-styled"><?= $model->getAttributeLabel($imageFieldName)?></label>
    <div class="preview-container" style="<?= (empty($model->image)?"display:none;":"")?>">
        <?php
        if (!empty($model->image)) {
            $imgSrc = $model->image->getUrl(400, 300);
        } else {
            $imgSrc = yii::$app->params["nopicPath"];
        }
        ?>
        <img id="recommend-image" src="<?= $imgSrc ?>" style="width: 100%;">
        <span class="remove"
              style="<?= (empty($model->image) ? "display:none;" : "") ?>"></span>
    </div>
    <div class="uniform-uploader" id="uniform-file-styled">
        <?php
        if (!empty($model->image)) {
            $source = $model->image->getUrl(400, 300);
        } else {
            $source = "Nincs file kiválasztva ...";
        }
        ?>
        <span class="filename" style="-moz-user-select: none;" title="<?= $source ?>">
                                        <?= $source ?>
                                    </span>
        <span title="<?= $source ?>" class="action btn bg-blue" data-toggle="modal"
              data-target="#image-selector-modal" style="-moz-user-select: none;">Tallóz</span>
    </div>
    <!--
    <p class="help-block ">
        <i class="glyphicon glyphicon-info-sign"></i>
        Válassz ajánlóképet
    </p>-->

    <?= \yii\helpers\Html::activeHiddenInput($model, $imageFieldName, ['class' => 'model-hidden-img-field']) ?>

</div>

<?php

$js = <<<JS
    $(function () {       
        $(".preview-container span").click(function () {
            removeImage(); 
        });        
    });
JS;
$this->registerJs($js, yii\web\View::POS_READY);

$js = <<<JS
    // Recommended image handler
    function addImage(id, url) {
        $(".model-hidden-img-field").val(id);
        $("#recommend-image").attr("src", url);
        $(".uniform-uploader .filename").html(url)
        $(".preview-container").show();        
        $(".preview-container .remove").show();
    }
    function removeImage() {
        $(".preview-container").hide();
        $(".model-hidden-img-field").val("");
        $("#recommend-image").attr("src", "/img/empty-pic.jpg");
        $(".uniform-uploader .filename").html("Nincs file kiválasztva ..."); 
    }    
JS;
$this->registerJs($js, yii\web\View::POS_HEAD);
?>

<!-- Modal image selector -->
<div class="modal fade" id="image-selector-modal" tabindex="-1" role="dialog"
     aria-labelledby="image-selector-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width:1100px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= $model->getAttributeLabel($imageFieldName)?></h5>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <iframe src="/admin/image/index?fromImageAttachWidget=1" style="border:1px solid #FDFDFD;" width="100%"
                        height="620">
                </iframe>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>