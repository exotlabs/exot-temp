<?php
namespace backend\widgets;

use common\models\ArticleSearch;
use Yii;


class RelatedArticlesWidget extends \yii\bootstrap\Widget
{

    public $parentId;
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $searchModel = new ArticleSearch();
        $searchModel->notRelatedId = $this->parentId;
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        $relatedSearchModel = new ArticleSearch();
        $relatedSearchModel->relatedId = $this->parentId;
        $relatedDataProvider = $relatedSearchModel->search([]);
        $relatedDataProvider->pagination = false;
        return $this->render('relatedArticles',
            ['searchModel' => $searchModel,
             'dataProvider' => $dataProvider,
             'parentId' => $this->parentId,
             'relatedDataProvider' => $relatedDataProvider,
        ]);
    }
}
