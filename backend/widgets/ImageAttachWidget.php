<?php
namespace backend\widgets;

use common\models\ArticleSearch;
use Yii;


class ImageAttachWidget extends \yii\bootstrap\Widget
{

    /**
     * The parent model from the editor
     *
     * @var null
     */
    public $model = null;

    /**
     * The field name in the database of the image
     *
     * @var string
     */
    public $imageFieldName = "image_id";

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->render('imageAttach',
            [
                'model' => $this->model,
                'imageFieldName' => $this->imageFieldName
            ]
        );
    }
}
