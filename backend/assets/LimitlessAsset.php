<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class LimitlessAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'exot-theme-limitless-2-0-1/global/css/icons/icomoon/styles.css',
        'exot-theme-limitless-2-0-1/global/css/icons/material/icons.css',
        'exot-theme-limitless-2-0-1/theme/css/bootstrap_limitless.css',
        'exot-theme-limitless-2-0-1/theme/css/layout.css',
        'exot-theme-limitless-2-0-1/theme/css/components.css',
        'exot-theme-limitless-2-0-1/theme/css/colors.css',

        // 'exot-theme-limitless-2-0-1/theme/css/core.css',
    ];
    public $js = [
        'exot-theme-limitless-2-0-1/global/js/main/bootstrap.bundle.min.js',
        'exot-theme-limitless-2-0-1/global/js/plugins/loaders/blockui.min.js',
        'exot-theme-limitless-2-0-1/global/js/plugins/visualization/d3/d3.min.js',
        'exot-theme-limitless-2-0-1/global/js/plugins/visualization/d3/d3_tooltip.js',
        'exot-theme-limitless-2-0-1/global/js/plugins/forms/styling/switchery.min.js',
        'exot-theme-limitless-2-0-1/global/js/plugins/forms/selects/bootstrap_multiselect.js',
        'exot-theme-limitless-2-0-1/global/js/plugins/ui/moment/moment.min.js',
        'exot-theme-limitless-2-0-1/global/js/plugins/pickers/daterangepicker.js',

        'exot-theme-limitless-2-0-1/global/js/plugins/ui/slinky.min.js',
        'exot-theme-limitless-2-0-1/global/js/plugins/buttons/ladda.min.js',

        'exot-theme-limitless-2-0-1/global/js/plugins/forms/styling/uniform.min.js',
        'exot-theme-limitless-2-0-1/global/js/plugins/media/fancybox.min.js',
        'exot-theme-limitless-2-0-1/global/js/demo_pages/login.js',                         // Need for the login page
        'exot-theme-limitless-2-0-1/global/js/demo_pages/components_buttons.js',
        'exot-theme-limitless-2-0-1/global/js/plugins/buttons/spin.min.js',

        'exot-theme-limitless-2-0-1/global/js/plugins/pickers/anytime.min.js',              // Datetime picker

        'exot-theme-limitless-2-0-1/global/js/plugins/editors/ckeditor/ckeditor.js',        // Editor
        'exot-theme-limitless-2-0-1/global/js/plugins/editors/ckeditor/config.js',

        'exot-theme-limitless-2-0-1/global/js/plugins/notifications/pnotify.min.js',        // Notify message

        //'ckfinder/ckfinder.js',                                                             //CKE media uploader and handler
        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js',      // Internallink select needed -- droppable

        'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js',                               // CKE own autocomplete plugin needs


        'exot-theme-limitless-2-0-1/global/js/plugins/forms/inputs/typeahead/handlebars.min.js',    // Need for alpaca
        'exot-theme-limitless-2-0-1/global/js/plugins/forms/selects/bootstrap_multiselect.js',      // Need for alpaca
        'exot-theme-limitless-2-0-1/global/js/plugins/forms/inputs/alpaca/alpaca.min.js',           // ALpaca special select

        'exot-theme-limitless-2-0-1/theme/js/app.js',
        'exot-theme-limitless-2-0-1/theme/js/custom.js',

    ];
    public $depends = [
        'backend\assets\AppAsset',
    ];
}
