<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => [User::ROLE_ADMIN],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['activate', 'adminindex', 'adminview', 'create', 'createorupdateuser',  'deactivate', 'delete',
                            'index', 'sendinvitation', 'update', 'userupdate', 'invite'],
                        'roles' => [User::ROLE_ADMIN],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['activate', 'adminindex', 'adminview', 'create', 'createorupdateuser',  'deactivate', 'delete',
                            'index', 'sendinvitation', 'update', 'userupdate'],
                        'roles' => [User::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    /**
     * The invite editor for users
     */
    public function actionInvite()
    {
        if (null !== Yii::$app->request->post('UserInvite')) {

            $inviteData = Yii::$app->request->post('UserInvite');
            $replacer = [' '=>',', ';'=>',', '\r' => ',', '\n' => ',', PHP_EOL=>',', ',,' =>','];
            $emailList = strtr( $inviteData['emails'], $replacer);
            $emailList = strtr( $emailList, $replacer);

            $emails = explode(',', $emailList);
            $wrongFormat = 0;
            $rightFormat = 0;
            $exist = 0;

            foreach ($emails as $email) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

                    $findUser = User::find()->where(['email' => $email])->one();
                    if (empty($findUser)) {
                        $findUser = new User();
                        $findUser->email = $email;
                        $findUser->username = $email;
                        $findUser->status = User::STATUS_PENDING;
                        $password = Yii::$app->security->generateRandomString(10);
                        $findUser->setPassword($password);
                        $findUser->generateConfirmToken();
                        $findUser->generateAuthKey();
                        $findUser->selectedAuthItem = $inviteData["role"];
                        if ($findUser->save()) {
                            $auth = Yii::$app->authManager;
                            $role = $auth->getRole($findUser->selectedAuthItem);
                            if ($role) {
                                $auth->assign($role, $findUser->id);
                                $rightFormat ++;
                            } else {
                                $wrongFormat ++;
                            }
                        } else {
                            $wrongFormat ++;
                        }

                    } else {
                        $exist ++;

                        /*
                        $findUser->emails = $inviteData['emails'];
                        $findUser->auth_item = $inviteData['auth_item'];
                        if (empty($findUser->user)){
                            if ( $findUser->save() ) {
                                Yii::$app->mailer->compose('@common/mail/registration',[ 'model' => $findUser, 'hash' => $findUser->hash, 'email' => $findUser->email])
                                    ->setFrom(Yii::$app->params['inviteEmailFrom'])
                                    ->setTo($findUser->email)
                                    ->setSubject("You've got an invite from Diageo!")
                                    ->send();

                                $rightFormat ++;
                            } else {
                                Yii::error($findUser->getErrors());
                            }
                        } else {
                            $exist ++;
                        }
                        */
                    }
                } else {
                    $wrongFormat ++;
                }
            }

            $response = [];
            if ($rightFormat > 0 || $exist > 0) {
                $response['success'] = true;
                $response['message'] = 'Import successful! Sent invitations:'.$rightFormat.'. Not valid emails: '.$wrongFormat.'. Already registered: '.$exist;

            } else {
                $response['success'] = false;
                $response['message'] = "There's no valid email address in the list.";
            }
            return json_encode($response);

        } else {
            return $this->render('invite', [
                'model' => null
            ]);
        }
    }

    /**
     * Lists all normal user, not admin and not partner user models.
     * @return mixed
     */
    public function actionIndex()
    {
        // The default list not shows the admin and partner users
        $searchModel = new UserSearch();
        $searchModel->listGroup = UserSearch::LIST_GROUP_USER;
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionAdminindex()
    {
        $searchModel = new UserSearch();
        $searchModel->listGroup = UserSearch::LIST_GROUP_ADMINS;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('../adminuser/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAdminview($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        $model->scenario = User::SCENARIO_ADMIN_CREATE;
        $model->selectedAuthItem = User::ROLE_WRITER;
        $model->status = User::STATUS_ACTIVE;
        if ($model->load(Yii::$app->request->post())) {
            $model->username = $model->email;
            $model->generateAuthKey(); // todo remove ~ not null
            $model->setPassword($model->passwordPlain);
            if ($model->save()) {
                $auth = Yii::$app->authManager;
                $auth->assign($auth->getRole($model->selectedAuthItem), $model->id);
                return $this->render('@frontend/views/admin/user/update', [
                    'model' => $model,
                    'successSave' => true
                ]);
            }
        }

        return $this->render('@frontend/views/admin/user/create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {        
        $model = $this->findModel($id);
        $successSave = false;
        $model->scenario = User::SCENARIO_ADMIN_UPDATE;
        $model->selectedAuthItem = $model->getFirstRoleName();
        if ($model->load(Yii::$app->request->post())) {
            $model->username = $model->email;
            if (!empty($model->passwordPlain)) {
                $model->setPassword($model->passwordPlain);
            }
            if ($model->selectedAuthItem !== $model->getFirstRoleName()) {
                $auth = Yii::$app->authManager;
                $auth->revokeAll($model->id);
                $auth->assign($auth->getRole($model->selectedAuthItem), $model->id);
            }
            if ($model->save()) {
                //return $this->redirect(['update', 'id' => $model->id]);
                $successSave = true;
            }

        }

        return $this->render('update', [
            'model' => $model,
            'successSave' => $successSave
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUserupdate($id)
    {
        $model = $this->findModel($id);
        $successSave = false;
        $model->scenario = User::SCENARIO_MODIFY;
        $model->selectedAuthItem = $model->getFirstRoleName();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $successSave = true;
                $model->username = $model->email;
                if (!empty($model->passwordPlain)) {
                    $model->setPassword($model->passwordPlain);
                }
                if ($model->selectedAuthItem !== $model->getFirstRoleName()) {
                    $auth = Yii::$app->authManager;
                    $auth->revokeAll($model->id);
                    $auth->assign($auth->getRole($model->selectedAuthItem), $model->id);
                }                
                if ($successSave && $model->save()) {
                    //return $this->redirect(['update', 'id' => $model->id]);
                    $transaction->commit();
                    $successSave = true;
                } else {
                    $successSave = false;
                    $transaction->rollBack();
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

        return $this->render('update', [
            'model' => $model,
            'successSave' => $successSave
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteUser();

        if (strpos(Yii::$app->request->getUrl(), "/adminuser/")) {
            return $this->redirect(['/admin/adminuser/adminindex']);
        } else {
            return $this->redirect(['/admin']);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
    }

    /**
     * Return all user for select
     *
     * @return []
     */
    public static function getUserList()
    {
        $users = User::find()->all();
        $results = [];
        foreach ($users as $user) {
            $results[$user->id] = $user->getFullName();
        }
        return $results;
    }

    /**
     * Activate a user
     *
     * @param int $id Id of user
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);
        $model->status = User::STATUS_ACTIVE;
        $model->save();
    }

    /**
     * Activate a user
     *
     * @param int $id Id of user
     */
    public function actionDeactivate($id)
    {
        $model = $this->findModel($id);
        $model->status = User::STATUS_INACTIVE;
        $model->save();
    }

    /**
     * Invitation request handler
     */
    public function actionSendinvitation($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = User::findOne($id);
        $user->generatePasswordResetToken();
        $success = $user->save();
        $link = Yii::$app->urlManager->createAbsoluteUrl(['felhasznaloi-megerosites/' . $user->password_reset_token]);
        if ($success && !Yii::$app->emailComponent->sendCreatePartnerEmail($user, $link)) {
            $user->addError('email', 'email küldése sikertelen!');
        }
        return [
            'success' => $success,
            'errors' => implode(',', $user->getErrorSummary(true))
        ];
    }

    private static function  calc30DayTimestamp() {
        return 30 * 24 * 60 * 60;
    }

}
