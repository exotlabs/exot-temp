<?php

namespace backend\controllers;

use common\models\ArticleRelated;
use common\models\User;
use Yii;
use common\models\Article;
use common\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN],
                    ],
                ],
                'except' => ['view']
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            if ($model->save()) {
                return $this->redirect(['update','id'=>$model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $successSave = false;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $successSave = true;
        }

        return $this->render('update', [
            'model' => $model,
            'successSave' => $successSave
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Adds a relation.
     *
     * @return array
     */
    public function actionAddrelated(){
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            $parent_id = Yii::$app->request->post('parent_id');

            $related = new ArticleRelated();
            $related->article_id = $parent_id;
            $related->related_id = $id;
            $related->created_user_id = Yii::$app->user->id;
            $related->created_at = time();
            $related->save();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($related->hasErrors()) {
                return [
                    'success' => false,
                    'errors' => $related->errors,
                    'code' => 400,
                ];
            }
            return [
                'success' => true,
                'code' => 200,
            ];
        }
    }

    /**
     * Removes a relations.
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionRemoverelated(){
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            $parent_id = Yii::$app->request->post('parent_id');

            $related = ArticleRelated::findOne(['article_id' => $parent_id, 'related_id' => $id]);
            $numberOfRowsDeleted = $related->delete();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($numberOfRowsDeleted === false) {
                return [
                    'success' => false,
                    'code' => 200,
                ];
            }
            return [
                'success' => true,
                'code' => 200,
            ];
        }
    }

}
