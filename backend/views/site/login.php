<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Belépés';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => [
            'class' => 'login-form'
        ]
    ]); ?>
    <div class="card mb-0">

        <div class="card-body">

            <div class="text-center mb-3">
                <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                <h5 class="mb-0"><?= \Yii::t('app', 'Login')?></h5>
                <span class="d-block text-muted">Adja meg belépési adatai</span>
            </div>

            <!--
            <div class="form-group form-group-feedback form-group-feedback-left required field-loginform-username">
                <input class="form-control"
                       placeholder="Username"
                       type="text"
                       name="LoginForm[username]"
                       id="loginform-username"
                       autofocus aria-required="true" aria-invalid="true"
                >
                <p class="help-block help-block-error"></p>
                <div class="form-control-feedback">
                    <i class="icon-user text-muted"></i>
                </div>
            </div>

            <div class="form-group form-group-feedback form-group-feedback-left required field-loginform-password">
                <input class="form-control"
                       placeholder="Password"
                       type="password"
                       name="LoginForm[password]"
                       id="loginform-password"
                       aria-required="true" aria-invalid="true"
                >
                <p class="help-block help-block-error"></p>
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
            </div>
            -->

            <?= $form->field($model, 'username', [
                    'template' => "<div class=\"form-group form-group-feedback form-group-feedback-left\">{input}<div class=\"form-control-feedback\">
                    <i class=\"icon-user text-muted\"></i>
                    </div>\n{hint}\n{error}</div>"
                ])->textInput(['autofocus' => true, 'placeholder' => 'felhasználónév'])->label(false) ?>

            <?= $form->field($model, 'password', [
                    'template' => "<div class=\"form-group form-group-feedback form-group-feedback-left\">{input}<div class=\"form-control-feedback\">
                    <i class=\"icon-lock2 text-muted\"></i>
                    </div>\n{hint}\n{error}</div>"
                ])->passwordInput(['placeholder' => 'jelszó']) ?>

            <div class="form-group d-flex align-items-center">

                <div class="form-check mb-0">
                    <label class="form-check-label" for="rememberMe">
                        <input id="rememberMe" name="LoginForm[rememberMe]" value="1" checked="" class="form-input-styled" data-fouc="" type="checkbox">
                        Emlékezz rám
                    </label>
                </div>

                <?php
                /*
                echo $form->field($model, 'rememberMe', [
                    'template' => '<div class="form-check mb-0">
                    <label class="form-check-label" for="rememberMe">
                        <div class="uniform-checker"><span>{input}</span></div>
                        {label}</label></div>'
                ])->checkbox([], false)
                */
                ?>

                <?= Html::a('Elfelejtette jelszavát?', ['site/request-password-reset'], ["class" => 'ml-auto']) ?>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Belépés <i class="icon-circle-right2 ml-2"></i></button>
            </div>

        </div>

    </div>


<?php ActiveForm::end(); ?>
