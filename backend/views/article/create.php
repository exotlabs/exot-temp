<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Article */

$this->title = 'Új cikk';
$this->params['breadcrumbs'][] = ['label' => 'Hírek, exkluzív tartalmak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render("../elements/_page_header.php", [
    "buttons" => [
    ]
]);

?>
<div class="article-create">

    <?= $this->render('_form', [
        'model' => $model,
        'successSave' => (isset($successSave)?$successSave:false)
    ]) ?>

</div>
