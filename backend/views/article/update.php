<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Article */

$this->title = 'Hír, exkluzív tartalom módosítás: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Hírek, exkluzív tartalmak', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Módosítás';

echo $this->render("../elements/_page_header.php", [
    "buttons" => [
        [
            "label" => "Új cikk",
            "class" => "icon-file-text2",
            "link" => "/admin/article/create"
        ],
        [
            "label" => "Preview",
            "class" => "icon-eye",
            "link" => "#",
            "attributes" => [
                'data-toggle' => 'modal',
                'data-target' => '#preview-modal',
            ]
        ]
    ]
]);

?>
<div class="article-update">

    <?= $this->render('_form', [
        'model' => $model,
        'successSave' => (isset($successSave)?$successSave:false)
    ]) ?>

</div>

<!-- Modal -->
<div class="modal fade" id="preview-modal" tabindex="-1" role="dialog" aria-labelledby="preview-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cikk preview</h5>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <iframe src="/cikk/<?=$model->id?>" style="border:1px solid #FDFDFD;" width="100%" height="400">
                </iframe>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>