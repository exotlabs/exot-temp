<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\widgets\RelatedArticlesWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Article */
/* @var $userTarget common\models\UserTarget */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="card-body">

    <div class="article-form">

        <?php $form = ActiveForm::begin(
            [
                'fieldConfig' => ['options' => ['class' => 'form-group']],
                'options' => [
                    'class' => (isset($successSave) && $successSave === true ? "save-success" : "")
                ]
            ]
        ); ?>

        <div class="row">
            <div class="col-md-9">

                <div class="panel card panel-default">
                    <div class="panel-body card-body">
                        <?= $form->field(
                            $model,
                            'title',
                            [
                                'template' => '
                                                {label}
                                                <div class="col-lg-12">
                                                    {input}
                                                    <div class="help-block"></div>
                                                </div>
                                            ',
                            ]
                        )->textInput(['maxlength' => true])->label($model->getAttributeLabel('title'), ['class' => 'col-form-label col-lg-2']) ?>
                        <?= $form->field(
                            $model,
                            'lead',
                            [
                                'template' => '
                                                {label}
                                                <div class="col-lg-12">
                                                    {input}
                                                    <div class="help-block"></div>
                                                </div>
                                            ',
                            ]
                        )->textarea(['maxlength' => true])->label($model->getAttributeLabel('lead'), ['class' => 'col-form-label col-lg-2']) ?>
                        <?= $form->field(
                            $model,
                            'description',
                            [
                                'template' => '
                                                {label}
                                                <div class="col-lg-12">
                                                    {input}
                                                    <div class="help-block"></div>
                                                </div>
                                            ',
                            ]
                        )->textarea(['maxlength' => true, 'class' => 'editor'])->label($model->getAttributeLabel('description'), ['class' => 'col-form-label col-lg-2']) ?>

                        <?php
                        /*
                       echo $form->field($model, 'description')->widget(dosamigos\tinymce\TinyMce::className(), [
                            'options' => ['rows' => 6],
                            'language' => 'hu',
                            'clientOptions' => [
                                'plugins' => [
                                    "advlist autolink lists link charmap print preview anchor",
                                    "searchreplace visualblocks code fullscreen",
                                    "insertdatetime media table contextmenu paste"
                                ],
                                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                            ]
                        ]);
                        */
                        ?>

                    </div>
                </div>
            </div>

            <div class="col-md-3">

                <div class="panel card panel-default">
                    <?php if (!$model->isNewRecord): ?>
                        <ul class="list-group list-group-flush border-bottom">
                            <li class="list-group-item">
                                <span class="font-weight-semibold">Létrehozva:</span>
                                <div class="ml-auto"><?= date('Y-m-d H:i:s', $model->created_at) ?></div>
                            </li>
                            <li class="list-group-item">
                                <span class="font-weight-semibold">Módosítva:</span>
                                <div class="ml-auto"><?= date('Y-m-d H:i:s', $model->updated_at) ?></div>
                            </li>
                        </ul>
                    <?php endif; ?>
                    <div class="panel-body card-body">
                        <div class="record-info">
                            <div class="form-group d-flex">
                                <?php if ($model->isNewRecord): ?>
                                    <?= Html::submitButton('<b><i class="icon-checkmark4"></i></b>' . Yii::t('frontend', 'Mentés'), ['class' => 'btn bg-teal-400 btn-labeled btn-labeled-left mr-auto']) ?>
                                    <?= Html::a('<i class="icon-cog3 mr-2"></i>' . Yii::t('frontend', 'Mégsem'), "/admin/article/index",
                                        [
                                            'class' => 'btn btn-outline-danger',
                                        ]);?>
                                <?php else: ?>
                                    <?= Html::submitButton('<b><i class="icon-checkmark4"></i></b>' . Yii::t('frontend', 'Mentés'), ['class' => 'btn bg-teal-400 btn-labeled btn-labeled-left mr-auto']) ?>
                                    <?= Html::a('<i class="icon-cog3 mr-2"></i>' . Yii::t('frontend', 'Törlés'), ['delete', 'id' => $model->id], [
                                        'class' => 'btn btn-outline-danger',
                                        'data' => [
                                            'confirm' => Yii::t('frontend', 'Biztosan törli az elemet?'),
                                            'method' => 'post',
                                        ],
                                    ]) ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="panel card panel-default">
                    <div class="panel-body card-body">

                        <?= $form->field(
                            $model,
                            'status',
                            [
                                'template' => '
                                                {label}
                                                <div class="">
                                                    {input}
                                                    <div class="help-block"></div>
                                                </div>
                                            ',
                            ]
                        )->dropDownList(
                            \common\models\Article::getStatusList(),
                            [
                                'class'  => 'article-select',
                            ]
                        )->label($model->getAttributeLabel('status'), ['class' => 'col-form-label']) ?>

                        <?= $form->field(
                            $model,
                            'type',
                            [
                                'template' => '
                                        {label}
                                        <div class="">
                                            {input}
                                            <div class="help-block"></div>									    
                                        </div>
                                    ',
                            ]
                        )->dropDownList(
                            \common\models\Article::getTypeList(),
                            [
                                'class'  => 'article-select',
                            ]
                        )->label($model->getAttributeLabel('type'), ['class' => 'col-form-label']) ?>

                        <?php
                        if ($model->isNewRecord) {
                            $model->published_user_id = Yii::$app->user->id;
                            $model->published_at = time();
                        }
                        ?>

                        <?= $form->field(
                            $model,
                            'published_user_id',
                            [
                                'template' => '
                                                {label}
                                                <div class="">
                                                    {input}
                                                    <div class="help-block"></div>
                                                </div>
                                            ',
                            ]
                        )->dropDownList(
                            \common\controllers\UserController::getUserList(),
                            [
                                'class'  => 'article-select',
                            ]
                        )->label('Szerző', ['class' => 'col-form-label']) ?>

                        <?= $form->field(
                            $model,
                            'publishedDate',
                            [
                                'template' => ' {label}
                                                <div class="input-group">
                                                    <span class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="icon-calendar3"></i>
                                                        </span>
                                                    </span>
                                                    {input}
                                                </div>
                                                <div class="help-block"></div>                                                                                                       
                                            ',
                            ]
                        )->textInput(['maxlength' => true], ["class" => "form-control"])->label($model->getAttributeLabel("published_at"), ['class' => 'col-form-label']) ?>

                    </div>
                </div>

                <div class="panel card panel-default">
                    <div class="panel-body card-body">
                        <?= \backend\widgets\ImageAttachWidget::widget(['model' => $model]); ?>
                    </div>
                </div>

            </div>

        </div>

        <?php ActiveForm::end(); ?>
        <div class="row">
            <div class="col-md-12">
                <!-- related -->
                <?php if (!$model->isNewRecord): ?>
                    <div class="panel card panel-default">
                        <div class="panel-body card-body">
                            <div class="article-related content">
                                <?= RelatedArticlesWidget::widget(['parentId' => $model->id]); ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <!-- related end-->


        <?php
        $js = <<<JS
    $(function () {
        
       $(".article-select").select2({
            width: '100%'       
       });          
        
        $( "#article-publisheddate").AnyTime_picker({
            format: "%Y-%m-%d %H:%i:%s"
        });
        
        var editor = CKEDITOR.replace('article-description', {
            filebrowserBrowseUrl: '/admin/image/index?ckfilemanager=1',
            height: 550,
        });
        
        if ($("form").length > 0) {
            if ($("form").hasClass("save-success")) {
                new PNotify({
                    title: 'Mentés sikers',
                    text: 'A megadott értékek mentésre kerültek!',
                    addclass: 'bg-success border-success'
                });      
            }
        }
        
        $('html,body').scrollTop(0);
        
    });
JS;
        $this->registerJs($js, yii\web\View::POS_READY);

?>

</div>