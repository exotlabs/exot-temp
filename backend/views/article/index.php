<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hírek, exkluzív tartalmak';
$this->params['breadcrumbs'][] = $this->title;

echo $this->render("../elements/_page_header.php", [
    "buttons" => [
        [
            "label" => "Új cikk",
            "class" => "icon-file-text2",
            "link" => "/admin/article/create"
        ]
    ]
]);
?>

<div class="article-index content">

    <div class="card">

    <?php Pjax::begin(["id" => "index-list", 'timeout' => 5000]); ?>

        <div class="datatable-header">
            <?php $form = ActiveForm::begin([
                'method'=>'get',
                'fieldConfig' => [
                    'options' => [
                        'tag' => false,
                    ],
                ],
            ]); ?>            <div id="DataTables_Table_1_filter" class="dataTables_filter">
                <label><span>Keresés:</span>
                    <?= $form->field($searchModel, 'search')
                        ->textInput(['placeholder'=>'keresés az adatokban ...','type'=>'search','class' => '', 'aria-controls'=>'DataTables_Table_1'])
                        ->label(false);?>                </label>
            </div>
            <?php ActiveForm::end(); ?>        </div>

    <?= \common\components\ExotGridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,     // Remove this if you want search input fields
  'options' => [
 'class' => ''
    ],
        'columns' => [
            [
                'attribute' => 'title',
                'headerOptions' => ['class' => 'sort'],
                'enableSorting' => true,
                'filter' => false,
                'format' => 'raw',
                'value' => function ($model) {
                    return '<a href="/admin/article/update/'.$model->id.'">'.$model->title.'</a>';
                }
            ],[
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'type',
                'headerOptions' => ['class' => 'sort'],
                'enableSorting' => true,
                'filter' => false,
                'value' => function ($model) {
                    return $model->getTypeLabel();
                }
            ],[
                'attribute' => 'created_at',
                'headerOptions' => ['class' => 'sort'],
                'enableSorting' => true,
                'filter' => false,
                'value' => function ($model) {
                    return date("Y.m.d H:i", $model->created_at);
                }
            ],[
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'published_user_id',
                'headerOptions' => ['class' => 'sort'],
                'enableSorting' => true,
                'filter' => false,
                'value' => function ($model) {
                    return $model->publishedUser->getFullName();
                }
            ],[
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'status',
                'headerOptions' => ['class' => 'sort'],
                'enableSorting' => true,
                'filter' => false,
                'value' => function ($model) {
                    return $model->getStatusLabel();
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Művelet',
                'buttons' =>
                    [
                        'view' => function () {
                            return '';
                        },
                        'update'=>function($url,$model,$key)
                        {
                            return Html::a( "<i class=\"icon-pencil7 \"></i>" , "/admin/article/update/".$model->id, [
                                'class' => 'text-dark',
                            ] ); //use Url::to() in order to change $url
                        },
                        'delete'=>function($url,$model,$key)
                        {
                            return Html::a( "<i class=\"icon-trash \"></i>" , "#", [
                                'class' => 'text-dark',
                                'data-toggle' => 'modal',
                                'data-target' => '#list-modal',
                                'data-url' => '/admin/article/delete/'.$model->id,
                                'onclick' => '$("#delete-btn").attr("data-url", $(this).data("url"));'
                            ] ); //use Url::to() in order to change $url
                            /*
                            return Html::a( "<i class=\"icon-trash \"></i>" , "/admin/article/delete/".$model->id, [
                                'class' => 'text-dark',
                            ] ); //use Url::to() in order to change $url
                            */
                            /*
                            return '<form action="/admin/article/delete/'.$model->id.'" method="post"><input type="hidden" name="id" value="'.$key.'">'
                                . '<input type="hidden" name="'.Yii::$app->request->csrfParam.'" value="'.Yii::$app->request->csrfToken.'">'
                                . '<button type="submit" class="dropdown-item delete-item"><i class="icon-trash"></i></button></form>';
                            */
                        }
                    ],
            ],
        ],
        'footerRowOptions' => [
            'class' => 'datatable-footer'
        ],
        'pager' => [
            'class' => '\yii\widgets\LinkPager',
            'disabledListItemSubTagOptions' => [
                'tag' => 'a',
                'class' => 'paginate-button'
            ],
            'activePageCssClass' => 'current',
            'maxButtonCount' => 5,
            'prevPageLabel' => '←',
            'prevPageCssClass' => 'previous',
            'nextPageLabel' => '→',
            'nextPageCssClass' => 'next',
            'linkContainerOptions' => [
                'tag' => 'li',
                'class' => 'paginate_button',
            ],
            'linkOptions' => [
                'class' => 'text-dark'
            ],
            'pageCssClass' => '',
            'options' => [
                'tag' => 'div',
                'class' => 'dataTables_paginate paging_simple_numbers'
            ]
        ]
    ]); ?>
    <?php Pjax::end(); ?>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="list-modal" tabindex="-1" role="dialog" aria-labelledby="list-modal-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Biztosan törli a kiválasztott elemet?</h5>
                </div>
                <div class="modal-body">
                    Törlés megerősítésével véglegesen törli az elemet.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Mégsem</button>
                    <button type="button" class="btn btn-primary" data-url="/" id="delete-btn" onclick="window.location.href=$(this).data('url')">Törlés</button>
                </div>
            </div>
        </div>
    </div>

</div>
