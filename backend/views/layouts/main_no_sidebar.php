<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use backend\assets\LimitlessAsset;

LimitlessAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<?php
$disableBackground = false;
if (isset($this->params['disable_background']) && $this->params['disable_background'] === true) {
    $disableBackground = true;
}

?>
<body class="<?= ($disableBackground?"no-background":"") ?>">
<?php $this->beginBody() ?>

<?php
$disableHeaderFooter = true;
if (!isset($this->params['disable_header_footer'])) {
    $disableHeaderFooter = false;
?>
    
    <div class="navbar navbar-expand-md navbar-dark">

        <div class="navbar-brand">
            <a href="/" class="d-inline-block">
                <img src="/img/eholtosag-logo.png" alt="">
            </a>
        </div>

    </div>

<?php
}
?>

<div class="page-content">
    <?php
    /*
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'About', 'url' => ['/site/about']],
        ['label' => 'Contact', 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    */
    ?>

    <div class="content-wrapper">
        <div class="<?= ($disableHeaderFooter === false ? "content" : "")?> d-flex justify-content-center <?= (isset($this->params['disable_align_items_center'])&&$this->params['disable_align_items_center']===true ? "" : "align-items-center") ?>">
            <?= $content ?>
        </div>

        <?php
        if (!isset($this->params['disable_header_footer'])) {
            ?>
            <div class="navbar navbar-expand-lg navbar-light">
                <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; Éholtóság <?= date('Y') ?>
                </span>
                </div>
            </div>
            <?php
        }
        ?>


    </div>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
