<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use backend\assets\LimitlessAsset;
use yii\widgets\Menu;
use common\models\User;

LimitlessAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand">

        <ul class="navbar-nav" style="margin-left:0;position:absolute;left:0;">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <a href="<?=Yii::$app->homeUrl?>" class="d-inline-block" style="margin-left:30px;">
            <img src="/img/eholtosag-logo.png" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <!--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>-->
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">


        <span class="navbar-text ml-md-3 mr-md-auto">
        </span>

        <ul class="navbar-nav">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-user rounded-circle"></i>
                    <span><?php echo Yii::$app->user->isGuest? 'Vendég': Yii::$app->user->identity->username ?></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <?php
                    echo Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            '<i class="icon-switch2"></i> Logout',
                            ['class' => 'dropdown-item']
                        )
                        . Html::endForm();
                    ?>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->
<div class="page-content">

    <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">
        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <?=Menu::widget([
                'items' => [
                    [
                        'label' => '<i class="icon-magazine"></i><span>Hírek, exkluzív tartalmak</span>',
                        'url' => ['/article/index'],
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN)
                    ],
                    [
                        'label' => '<i class="icon-file-picture"></i><span>Képek</span>',
                        'url' => ['/image/index'],
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN)
                    ],
                    [
                        'label' => '<i class="icon-user"></i><span>Felhasználók</span>',
                        'url' => '/admin/user/index',
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN)
                    ],
                    [
                        'label' => '<i class="icon-mail5"></i><span>Meghívó küldése</span>',
                        'url' => '/admin/user/invite',
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN)
                    ],
                    [
                        'label' => '<i class="icon-user-lock"></i><span>Adminisztrátorok</span>',
                        'url' => ['/adminuser/adminindex'],
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN)
                    ],
                ],
                'activeCssClass'=>'active nav-item-expanded nav-item-open',
                'encodeLabels' => false,
                'options' => [
                    'class' => 'nav nav-sidebar',
                    'data-nav-type' => 'accordion'
                ],
                'submenuTemplate' => "\n<ul class='nav nav-group-sub'>\n{items}\n</ul>\n",
                'linkTemplate' => '<a href="{url}" class="nav-link">{label}</a>',
                'itemOptions'=>array('class'=>'nav-item'),
                'activateParents'=>true,
            ]);?>
        </div>

    </div>

    <div class="content-wrapper">
        <?= $content ?>
    </div>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
