<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card-body">

    <div class="user-form">

        <?php $form = ActiveForm::begin(
            [
                'fieldConfig' => ['options' => ['class' => 'form-group row']],
                'options' => [
                    'class' => (isset($successSave)&&$successSave===true?"save-success":"")
                ]
            ]
        ); ?>
        <?php // echo $form->errorSummary($model); ?>
        <div class="row">

            <div class="col-md-9">


                <div class="panel card panel-default">
                    <div class="panel-body card-body">

                        <?= $form->errorSummary($model); ?>
                        <?= $form->field(
                            $model,
                            'email',
                            [
                                'template' => "
									{label}
									<div class=\"col-lg-10\">
									    {input}
                                        <!--<div class=\"help-block\">{error}</div>-->
									</div>
								",
                            ]
                        )->textInput(['maxlength' => true])->label($model->getAttributeLabel('email'), ['class'=>'col-form-label col-lg-2']) ?>

                        <?= $form->field(
                            $model,
                            'first_name',
                            [
                                'template' => "
									{label}
									<div class=\"col-lg-10\">
									    {input}
                                        <!--<div class=\"help-block\">{error}</div>-->
									</div>
								",
                            ]
                        )->textInput(['maxlength' => true])->label($model->getAttributeLabel('first_name'), ['class'=>'col-form-label col-lg-2']) ?>

                        <?= $form->field(
                            $model,
                            'family_name',
                            [
                                'template' => "
									{label}
									<div class=\"col-lg-10\">
									    {input}
                                        <!--<div class=\"help-block\">{error}</div>-->
									</div>
								",
                            ]
                        )->textInput(['maxlength' => true])->label($model->getAttributeLabel('family_name'), ['class'=>'col-form-label col-lg-2']) ?>

                        <?= $form->field(
                            $model,
                            'passwordPlain',
                            [
                                'template' => '
									{label}
									<div class="col-lg-10">
									    {input}
                                        <!--<div class=\"help-block\">{error}</div>-->
									</div>
								',
                            ]
                        )->passwordInput()->label($model->getAttributeLabel('passwordPlain'), ['class'=>'col-form-label col-lg-2']) ?>

                        <?= $form->field(
                            $model,
                            'passwordConfirm',
                            [
                                'template' => '
									{label}
									<div class="col-lg-10">
									    {input}
                                        <!--<div class="help-block">{error}</div>-->
									</div>
								',
                            ]
                        )->passwordInput()->label($model->getAttributeLabel('passwordConfirm'), ['class'=>'col-form-label col-lg-2']) ?>

                    </div>
                </div>

            </div>

            <div class="col-md-3">
                <div class="panel card panel-default">
                    <?php if (!$model->isNewRecord): ?>
                        <ul class="list-group list-group-flush border-bottom">
                            <li class="list-group-item">
                                <span class="font-weight-semibold">Létrehozva:</span>
                                <div class="ml-auto"><?= date('Y-m-d H:i:s', $model->created_at) ?></div>
                            </li>
                            <li class="list-group-item">
                                <span class="font-weight-semibold">Módosítva:</span>
                                <div class="ml-auto"><?= date('Y-m-d H:i:s', $model->updated_at) ?></div>
                            </li>
                        </ul>
                    <?php endif; ?>

                    <div class="panel-body card-body">
                        <div class="record-info">
                            <div class="form-group d-flex">
                                <?php if ($model->isNewRecord): ?>
                                    <?= Html::submitButton(Yii::t('frontend', 'Mentés'), ['class' => 'btn btn-primary']) ?>
                                    <?= Html::a(Yii::t('frontend', 'Mégsem'), ['adminindex'], ['class' => 'btn btn-default']) ?>
                                <?php else: ?>

                                    <?= Html::submitButton('<b><i class="icon-checkmark4"></i></b>' . Yii::t('frontend', 'Mentés'), ['class' => 'btn bg-teal-400 btn-labeled btn-labeled-left mr-auto']) ?>

                                    <?= Html::a('<i class="icon-cog3 mr-2"></i>' . Yii::t('frontend', 'Törlés'), ['delete', 'id' => $model->id], [
                                        'class' => 'btn btn-outline-danger',
                                        'data' => [
                                            'confirm' => Yii::t('frontend', 'Biztosan törli az elemet?'),
                                            'method' => 'post',
                                            'disabled' => !Yii::$app->user->can(User::ROLE_ADMIN)
                                        ],

                                    ]) ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel card panel-default">
                    <div class="panel-body card-body">

                        <?= $form->field(
                            $model,
                            'selectedAuthItem',
                            [
                                'template' => "
                                {label}
                                <div class=\"col-lg-12\">
                                    {input}
                                    <div class=\"help-block\">{error}</div>				    
                                </div>
                            ",
                            ]
                        )->dropDownList(
                            User::getAdminRoleList(),
                            [
                                'class'  => 'user-select',
                            ]
                        )->label($model->getAttributeLabel('selectedAuthItem'), ['class'=>'col-form-label col-lg-12']) ?>

                        <?= $form->field(
                            $model,
                            'status',
                            [
                                'template' => "
                                    {label}
                                    <div class=\"col-lg-12\">
                                        {input}
                                        <div class=\"help-block\">{error}</div>
                                    </div>
                                ",
                            ]
                        )->radioList(
                            [
                                User::STATUS_ACTIVE => "Aktív",
                                User::STATUS_DELETED => "Törölve",
                            ],
                            [
                                'item' => function($index, $label, $name, $checked, $value) {
                                    $return = '<div class="col-sm form-check-inline">';
                                    $return .= '<label class="form-check-label">';
                                    $return .= '<input '.($checked?'checked':'').' class="user-radio" type="radio" name="' . $name . '" value="' . $value . '">';
                                    $return .= $label;
                                    $return .= '</label>';
                                    $return .= '</div>';
                                    return $return;
                                },
                                'class' => 'row col-lg-12'
                            ]
                        )->label($model->getAttributeLabel('status'), ['class'=>'col-form-label col-lg-12']) ?>
                    </div>
                </div>

            </div>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$js = <<<JS
    $("input.user-radio:radio").uniform();

   $(".user-select").select2({
        width: '100%'       
   });  

    $(function () {
        if ($("form").length > 0) {
            if ($("form").hasClass("save-success")) {
                new PNotify({
                    title: 'Mentés sikers',
                    text: 'A megadott értékek mentésre kerültek!',
                    addclass: 'bg-success border-success'
                });      
            }
        }
    });
JS;
$this->registerJs($js, yii\web\View::POS_READY);
?>

</div>
