<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
?>
<div class="content-wrapper">

    <!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4> <span class="font-weight-semibold">Invite user</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card">
        <div class="card-body">
            <?php Pjax::begin([
                'timeout' => 5000
            ]); ?>
            <?php $form = ActiveForm::begin([
                'id' => 'invite-users',
                'options' => [
                    'data-pjax' => false
                ]
            ]); ?>
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Invite users</legend>
                </fieldset>
                <div class="import-message">

                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Email címek</label>
                    <div class="col-lg-10">
                        <textarea name="UserInvite[emails]" rows="3" cols="3" class="form-control" placeholder="pl.: alma@fusen.hu,korte@fusen.hu"></textarea>
                    </div>
                </div>


                <div class="form-group row">
                	<label class="col-form-label col-lg-2">Role for the users</label>
                	<div class="col-lg-10">

                        <?= Html::dropDownList("UserInvite[role]", null,
                            \common\models\User::getNormalRoleList(),[
                                "class" => "limitless-select",
                            ])?>

                    </div>
                </div>
                <div class="form-group float-right">
                   <?php if( isset( $_SERVER['HTTP_REFERER'] )) { ?>
                        <?= Html::a('Back', $_SERVER['HTTP_REFERER'], ['class' => 'btn']) ?>
                    <?php } ?>  
                    <button type="button" id="send_invite" class="btn btn-primary" >Send invite <i class="icon-paperplane ml-2"></i></button>
				</div>
            <?php ActiveForm::end(); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

<?php
$js = <<<JS
$(function () {
    $('body').on({
        click: function(e){
            if (!$(this).hasClass('disabled')) {
                $(this).prepend('<i class="icon-spinner2 spinner"></i> ').addClass('disabled');
                $('#invite-users').submit();
            } else {
                e.preventDefault();
            }
        }
    }, '#send_invite');    
    
    $('body').on({
        submit: function(e) {
            e.preventDefault();
            $.post('', $(this).serialize(), function(response) {
                $('#send_invite').removeClass('disabled');
                $('#send_invite .spinner').remove();
                $('textarea').val('');
                response = $.parseJSON(response);
                if (response.success) {
                    $('.import-message').html('<div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible"><button type="button" class="close" data-dismiss="alert"><span>×</span></button>'+response.message+'</div>');
                } else {
                    $('.import-message').html('<div class="alert alert-danger alert-styled-left alert-arrow-left alert-dismissible"><button type="button" class="close" data-dismiss="alert"><span>×</span></button>'+response.message+'</div>');
                }
            });
        }
    }, '#invite-users');    
});
JS;

$this->registerJs($js, yii\web\View::POS_READY);

?>