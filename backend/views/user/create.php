<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\User */

if (strpos(Yii::$app->request->getUrl(), "/adminuser/")) {
    $this->title = Yii::t('frontend', 'Új adminisztrátor');
    $this->params['breadcrumbs'][] = ['label' => 'Adminisztrátorok', 'url' => ['adminindex']];
} else {
    $this->title = Yii::t('frontend', 'Új felhasználó');
    $this->params['breadcrumbs'][] = ['label' => 'Felhasználók', 'url' => ['index']];
}

$this->params['breadcrumbs'][] = $this->title;

echo $this->render("/admin/elements/_page_header.php", [
    "buttons" => [
    ]
]);

?>
<div class="user-create">

    <?php

    if (strpos(Yii::$app->request->getUrl(), "/adminuser/")) {
        echo $this->render('_form', [
            'model' => $model,
            'successSave' => (isset($successSave)?$successSave:false)]);

    } else {
        echo $this->render('_userForm', [
            'model' => $model,
            'successSave' => (isset($successSave)?$successSave:false)]);
    }

    ?>

</div>
