<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */


if (strpos(Yii::$app->request->getUrl(), "/adminuser/")) {
    $this->title = Yii::t('frontend', 'Adminisztrátor módosítás: ' . $model->id, [
        'nameAttribute' => '' . $model->id,
    ]);
    $this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Adminisztrátorok'), 'url' => ['adminindex']];
} else {
    $this->title = Yii::t('frontend', 'Felhasználó módosítás: ' . $model->id, [
        'nameAttribute' => '' . $model->id,
    ]);
    $this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Felhasználók'), 'url' => '/admin/user/index'];
}

$this->params['breadcrumbs'][] = Yii::t('frontend', 'Módosítás');

echo $this->render("../elements/_page_header.php", [
    "buttons" => [
    ]
]);

?>
<div class="user-update">

    <?php

    if (strpos(Yii::$app->request->getUrl(), "/adminuser/")) {
        echo $this->render('_form', [
            'model' => $model,
            'successSave' => (isset($successSave)?$successSave:false)]);

    } else {
        echo $this->render('_userForm', [
            'model' => $model,
            'successSave' => (isset($successSave)?$successSave:false)]);
    }

    ?>


</div>
