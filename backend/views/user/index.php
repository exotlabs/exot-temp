<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('frontend', 'Felhasználók');
$this->params['breadcrumbs'][] = $this->title;

echo $this->render("../elements/_page_header.php", [
    "buttons" => [
    ]
]);

?>

<div class="user-index content">
    <div class="card">

    <?php \yii\widgets\Pjax::begin(["id" => "index-list", 'timeout' => 5000])?>

    <div class="card-body">
        <?php $form = ActiveForm::begin([
                'method'=>'post',
                'fieldConfig' => [
                    'options' => [
                        'tag' => false,
                    ],
                ],
            ]); ?>
        <div class="row">
            <div class="col-lg-3">
                <div class="row">
                    <label class="col-form-label col-lg-12" for="user_name">Felhasználó neve vagy azonosítója</label>
                    <div class="col-lg-12">
                        <input type="text" id="user_name" class="form-control" name="UserSearch[name]" value="<?= $searchModel->name ?>" maxlength="255" aria-required="true">
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="row">
                    <label class="col-form-label col-lg-12" for="user_email">Felhasználó email címe</label>
                    <div class="col-lg-12">
                        <input type="text" id="user_email" class="form-control" name="UserSearch[email]" value="<?= $searchModel->email ?>" maxlength="255" aria-required="true">
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="row">
                    <label class="col-form-label col-lg-12" for="user_created_at">Regisztráció időpontja (tól-ig)</label>
                    <div class="col-lg-12 input-group">
                        <input type="text" id="user_created_at" class="form-control" name="UserSearch[created_at_from_to]" value="<?= $searchModel->created_at_from_to ?>" maxlength="255" aria-required="true">
                        <span class="input-group-append">
                            <span class="input-group-text"><i class="icon-calendar22"></i></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="row">
                    <label class="col-form-label col-lg-12" for="username">Státusz</label>
                    <div class="col-lg-12">
                        <?= Html::dropDownList("UserSearch[status]", $searchModel->status,
                            User::getStatusListAndEmpty()
                        ,[
                            "id" => "user_status",
                            "class" => "limitless-select"
                        ])?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <br>
                <?= Html::submitButton('<b><i class="icon-checkmark4"></i></b>' . Yii::t('frontend', 'Keresés'), ['class' => 'btn bg-teal-400 btn-labeled btn-labeled-left mr-auto']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

    <?= \common\components\ExotGridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,    // Remove this if you want search input fields
        'options' => [
            'class' => '',
        ],
        'columns' => [
            [
                'attribute' => 'fullName',
                'headerOptions' => [
                    'class' => 'sort',
                ],
                'enableSorting' => true,
                'filter' => false,
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->status === common\models\User::STATUS_DELETED) {
                        return "<b ><a class='not-set' data-pjax=0 href='/admin/user/userupdate/".$model->id."'>TÖRÖLT USER ID: ".$model->id."</a></b>";
                    } else {
                        return '<div><a data-pjax=0 href="/admin/user/userupdate/'.$model->id.'">'.$model->getFullName().'</a></div>';
                    }
                }
            ],
            [
                'label' => 'Regisztráció',
                'attribute' => 'created_at',
                'headerOptions' => [
                    'class' => 'sort',
                ],
                'enableSorting' => true,
                'filter' => false,
                'value' => function ($model) {
                    return date("Y.m.d H:i", $model->created_at);
                }
            ],
            [
                'attribute' => 'email',
                'headerOptions' => [
                    'class' => 'sort',
                ],
                'enableSorting' => true,
                'filter' => false,
                'value' => function ($model) {
                    if ($model->status === common\models\User::STATUS_DELETED) {
                        return "-";
                    } else {
                        return $model->email;
                    }
                }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'status',
                'headerOptions' => [
                    'class' => 'sort',
                ],
                'enableSorting' => true,
                'filter' => false,
                'value' => function ($model) {
                    return $model->getStatusLabel();
                }
            ],
        ],
        'footerRowOptions' => [
            'class' => 'datatable-footer'
        ],
        'pager' => [
            'class' => '\yii\widgets\LinkPager',
            'disabledListItemSubTagOptions' => [
                'tag' => 'a',
                'class' => 'paginate-button'
            ],
            'activePageCssClass' => 'current',
            'maxButtonCount' => 5,
            'prevPageLabel' => '←',
            'prevPageCssClass' => 'previous',
            'nextPageLabel' => '→',
            'nextPageCssClass' => 'next',
            'linkContainerOptions' => [
                'tag' => 'li',
                'class' => 'paginate_button',
            ],
            'linkOptions' => [
                'class' => 'text-dark'
            ],
            'pageCssClass' => '',
            'options' => [
                'tag' => 'div',
                'class' => 'dataTables_paginate paging_simple_numbers'
            ]
        ]
    ]); ?>

    <?php \yii\widgets\Pjax::end()?>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="list-modal" tabindex="-1" role="dialog" aria-labelledby="list-modal-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Biztosan törli a kiválasztott elemet?</h5>
                </div>
                <div class="modal-body">
                    Törlés megerősítésével véglegesen törli az elemet.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Mégsem</button>
                    <button type="button" class="btn btn-primary" data-url="/" id="delete-btn" onclick="window.location.href=$(this).data('url')">Törlés</button>
                </div>
            </div>
        </div>
    </div>

</div>

<?php
$js = <<<JS
$(function () {
    // Show calendars on left
    function initUserDateRangePickers() {
        $('#user_created_at, #user_package_date').daterangepicker({
            opens: 'left',
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-light',
            autoUpdateInput : false,
            locale: {
                format: 'YYYY-MM-DD'
            }        
        }, function(start_date, end_date) {
                this.element.val(start_date.format('YYYY-MM-DD') + ' - ' + end_date.format('YYYY-MM-DD'));
        });    
    }
    
    initUserDateRangePickers();
    
    $(document).on('pjax:end', function(data, status, xhr, options) {
        initUserDateRangePickers();        
    });      

});        
    
JS;

$this->registerJs($js, yii\web\View::POS_READY);

?>