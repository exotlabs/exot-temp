<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use common\models\SubscriptionInfo;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="card-body">

    <div class="user-form">
        <?php $form = ActiveForm::begin(
            [
                'fieldConfig' => ['options' => ['class' => 'form-group row']],
                'options' => [
                    'class' => (isset($successSave)&&$successSave===true?"save-success":"")
                ]
            ]
        ); ?>

        <div class="row">

            <div class="col-md-9">

                <?php
                if (!empty($model->specialError)) {
                ?>
                    <div class="alert alert-warning alert-styled-left alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                        <?= $model->specialError ?>
                    </div>
                <?php
                }
                ?>

                <div class="panel card panel-default">
                    <div class="panel-body card-body">

                        <h3 style="border-bottom:2px solid #DDD;padding-bottom:10px;margin-bottom: 20px;">Regisztrációs adatok</h3>

                        <?php
                        if ($model->status === User::STATUS_DELETED) {
                        ?>
                            <p class="not-set" style="font-size:18px;font-style:inherit;">Törölt felhasználó. Törlés időpontja:<?=date("Y.m.d H:i", $model->updated_at);?></p>
                        <?php
                        } else {
                            ?>

                            <?= $form->errorSummary($model); ?>

                            <div class="row">

                                <div class="col-lg-4">
                                    <div class="">
                                        <?= $form->field(
                                            $model,
                                            'family_name',
                                            [
                                                'template' => "
                                                    {label}
                                                    <div class=\"col-lg-12\">
                                                        {input}
                                                        <div class=\"help-block\">{error}</div>
                                                    </div>
                                                ",
                                            ]
                                        )->textInput(['maxlength' => true])->label($model->getAttributeLabel('family_name'), ['class' => 'col-form-label col-lg-12']) ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="">
                                        <?= $form->field(
                                            $model,
                                            'first_name',
                                            [
                                                'template' => "
                                                    {label}
                                                    <div class=\"col-lg-12\">
                                                        {input}
                                                        <div class=\"help-block\">{error}</div>
                                                    </div>
                                                ",
                                            ]
                                        )->textInput(['maxlength' => true])->label($model->getAttributeLabel('first_name'), ['class' => 'col-form-label col-lg-12']) ?>

                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="">
                                        <?= $form->field(
                                            $model,
                                            'email',
                                            [
                                                'template' => "
                                                    {label}
                                                    <div class=\"col-lg-12\">
                                                        {input}
                                                        <div class=\"help-block\">{error}</div>
                                                    </div>
                                                ",
                                            ]
                                        )->textInput(['maxlength' => true])->label($model->getAttributeLabel('email'), ['class' => 'col-form-label col-lg-12']) ?>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="">
                                        <?= $form->field(
                                            $model,
                                            'gender',
                                            [
                                                'template' => "
                                                    {label}
                                                    <div class=\"col-lg-12\">
                                                        {input}
                                                        <div class=\"help-block\">{error}</div>				    
                                                    </div>
                                                ",
                                            ]
                                        )->dropDownList(
                                            User::getGenderList(),
                                            [
                                                'class'  => 'limitless-select',
                                            ]
                                        )->label($model->getAttributeLabel('gender'), ['class'=>'col-form-label col-lg-12']) ?>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="">
                                        <?= $form->field(
                                            $model,
                                            'age',
                                            [
                                                'template' => "
                                                    {label}
                                                    <div class=\"col-lg-12\">
                                                        {input}
                                                        <div class=\"help-block\">{error}</div>
                                                    </div>
                                                ",
                                            ]
                                        )->textInput(['maxlength' => true])->label($model->getAttributeLabel('age'), ['class' => 'col-form-label col-lg-12']) ?>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="">
                                        <?= $form->field(
                                            $model,
                                            'zip',
                                            [
                                                'template' => "
                                                    {label}
                                                    <div class=\"col-lg-12\">
                                                        {input}
                                                        <div class=\"help-block\">{error}</div>
                                                    </div>
                                                ",
                                            ]
                                        )->textInput(['maxlength' => true])->label($model->getAttributeLabel('zip'), ['class' => 'col-form-label col-lg-12']) ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="">
                                        <?= $form->field(
                                            $model,
                                            'passwordPlain',
                                            [
                                                'template' => '
                                                    {label}
                                                    <div class="col-lg-12">
                                                        {input}
                                                        <div class=\"help-block\">{error}</div>
                                                    </div>
                                                ',
                                            ]
                                        )->passwordInput()->label($model->getAttributeLabel('passwordPlain'), ['class' => 'col-form-label col-lg-12']) ?>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="">
                                        <?= $form->field(
                                            $model,
                                            'passwordConfirm',
                                            [
                                                'template' => '
                                                    {label}
                                                    <div class="col-lg-12">
                                                        {input}
                                                        <div class="help-block">{error}</div>
                                                    </div>
                                                ',
                                            ]
                                        )->passwordInput()->label($model->getAttributeLabel('passwordConfirm'), ['class' => 'col-form-label col-lg-12']) ?>

                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="">
                                        <?= $form->field(
                                            $model,
                                            'status',
                                            [
                                                'template' => "
                                                    {label}
                                                    <div class=\"col-lg-12\">
                                                        {input}
                                                        <div class=\"help-block\">{error}</div>
                                                    </div>
                                                ",
                                            ]
                                        )->radioList(
                                            [
                                                User::STATUS_ACTIVE => "Aktív",
                                                User::STATUS_DELETED => "Törölve",
                                            ],
                                            [
                                                'item' => function($index, $label, $name, $checked, $value) {
                                                    $return = '<div class="col-sm form-check-inline">';
                                                    $return .= '<label class="form-check-label">';
                                                    $return .= '<input '.($checked?'checked':'').' class="limitless-radio" type="radio" name="' . $name . '" value="' . $value . '">';
                                                    $return .= $label;
                                                    $return .= '</label>';
                                                    $return .= '</div>';
                                                    return $return;
                                                },
                                                'class' => 'row col-lg-12'
                                            ]
                                        )->label($model->getAttributeLabel('status'), ['class'=>'col-form-label col-lg-12']) ?>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                        ?>

                    </div>
                </div>

            </div>

            <div class="col-md-3">
                <?php
                if ($model->status !== User::STATUS_DELETED) {
                ?>
                    <div class="panel card panel-default">
                        <div class="panel-body card-body">
                            <div class="record-info">
                                <div class="form-group d-flex">
                                    <?php if ($model->isNewRecord): ?>
                                        <?= Html::submitButton(Yii::t('frontend', 'Mentés'), ['class' => 'btn btn-primary', 'name'=>'User[subaction]', 'value'=>'insert']) ?>
                                        <?= Html::a(Yii::t('frontend', 'Mégsem'), ['adminindex'], ['class' => 'btn btn-default']) ?>
                                    <?php else: ?>

                                        <?= Html::submitButton('<b><i class="icon-checkmark4"></i></b>' . Yii::t('frontend', 'Mentés'),
                                            [
                                                'class' => 'btn bg-teal-400 btn-labeled btn-labeled-left mr-auto',
                                                'id' => 'form-sender-btn',
                                                'name'=>'User[subaction]', 'value'=>'update'
                                            ]) ?>

                                        <?= Html::a('<i class="icon-cog3 mr-2"></i>' . Yii::t('frontend', 'Törlés'), ['delete', 'id' => $model->id], [
                                            'class' => 'btn '.(!Yii::$app->user->can(User::ROLE_ADMIN)?"disabled bg-secondary":"btn-outline-danger"),
                                            'data' => [
                                                'confirm' => Yii::t('frontend', 'Biztosan törli az elemet?'),
                                                'method' => 'post',
                                            ],
                                        ]) ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <div class="panel card panel-default">
                    <div class="panel-body card-body">

                            <div class="row mb-2">
                                <div class="col-sm font-weight-bold">
                                    Reg.dátum
                                </div>
                                <div class="col-sm">
                                    <?=date("Y.m.d H:i", $model->updated_at);?>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-sm font-weight-bold">
                                    Státusz
                                </div>
                                <div class="col-sm">
                                    <?= $model->getStatusLabel() ?>
                                </div>
                            </div>

                    </div>
                </div>

            </div>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php

$js = <<<JS
    function saveForm(item) {
        $(item).prev("button").trigger("click");
        $("#form-sender-btn").click();
        
    }
JS;
$this->registerJs($js, yii\web\View::POS_HEAD);

$js = <<<JS
    $(function () {
        
        if ($("form").length > 0) {
            if ($("form").hasClass("save-success")) {
                new PNotify({
                    title: 'Mentés sikers',
                    text: 'A megadott értékek mentésre kerültek!',
                    addclass: 'bg-success border-success'
                });      
            }
        }

    });
JS;
$this->registerJs($js, yii\web\View::POS_READY);

?>

</div>
