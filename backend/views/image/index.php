<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\imageuploader\ExotImageUploader;

//use frontend\assets\LimitlessFileUploadAsset;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Képek';
$this->params['breadcrumbs'][] = $this->title;

$disableHeaderFooter = true;
if (!isset($this->params['disable_header_footer'])) {
    $disableHeaderFooter = false;
    echo $this->render("../elements/_page_header.php", [
        "buttons" => []
    ]);
}
?>

<div class="image-index <?= ($disableHeaderFooter === false ? "content" : "")?>">
    <?php
        echo ExotImageUploader::widget([
            'pluginOptions'=>[
                'allowedFileExtensions'=>['jpg','jpeg','png']
            ],
            'options'=>[
                'accept'=>'image/*', 
                'multiple'=>true
            ],
        ]);
    ?>

<?php Pjax::begin([
    'id' => 'index-list', 
    'timeout' => 5000,
    'clientOptions' => ['method' => 'POST']
]); ?>
    <div class="card">
        <div class="datatable-header">
            <?php $form = ActiveForm::begin([
                'method'=>'get',
                'fieldConfig' => [
                    'options' => [
                        'tag' => false,
                    ],
                ],
            ]); ?>            
            <div id="DataTables_Table_1_filter" class="dataTables_filter">
                <label><span>Keresés:</span>
                    <?= $form->field($searchModel, 'search')
                        ->textInput([
                            'placeholder'=>'keresés a fájlnévben  ...',
                            'type'=>'search',
                            'class' => '', 'aria-controls'=>'DataTables_Table_1'
                        ])
                        ->label(false);?>
                </label>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    <?= \common\components\ExotGridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,     // Remove this if you want search input fields
        'options' => [
        'class' => ''
            ],
                'columns' => [
                    [
                        'format' => 'html',
                        'label' => 'Preview',
                        'value' => function($model) {
                            return Html::a(
                                Html::img($model->getUrl( 600, 450), ['alt' => $model->file_name, 'class' => 'thumb']), 
                                $model->getUrl( 600, 450),
                                [
                                    'class' => 'popup-lightbox',
                                ]
                            );
                            
                        }
                    ],
                    [
                        'attribute' => 'file_name',
                        'headerOptions' => ['class' => 'sort'],
                        'enableSorting' => true,
                        'filter' => false,
                    ],
                    [
                        'header' => 'Méret',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->width . ' X ' . $model->height;
                        }
                    ],
                    [
                        'format' => 'html',
                        'label' => 'Feltöltve',
                        'headerOptions' => ['class' => 'sort'],
                        'enableSorting' => true,
                        'value' => function($model) {
                            return  date('Y-m-d H:i', $model->created_at);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Művelet',
                        'buttons' =>
                            [
                                'view' => function () {
                                    return '';
                                },
                                'update'=>function($url,$model,$key)
                                {
                                    return '';
                                },
                                'delete'=>function($url,$model,$key)
                                {
                                    if (!empty(Yii::$app->request->get("fromImageAttachWidget"))) {
                                        // Special article recommend image handler
                                        return '<button type="button" class="badge badge-success" 
                                            onclick="
                                                parent.addImage('.$model->id.',\''.$model->getUrl(400, 300).'\');
                                                $(parent.document.getElementById(\'image-selector-modal\')).find(\'button.close\').trigger(\'click\');
                                            "
                                        >Csatolás</button>';
                                    } else if (!empty(Yii::$app->request->get('CKEditor'))){
                                        /* if works as ck editor plugin */
                                        return '<button onclick="returnFileUrl('.$model->id.',\''.$model->getUrl(400, 300).'\');" type="button" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-plus-circle2"></i></b> Kiválaszt</button>';
                                    } else {
                                        return Html::a( "<i class=\"icon-trash \"></i>" , "#", [
                                            'class' => 'text-dark',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#list-modal',
                                            'data-url' => '/admin/image/delete/'.$model->id,
                                            'onclick' => '$("#delete-btn").attr("data-url", $(this).data("url"));'
                                        ] ); //use Url::to() in order to change $url
                                    }
                                }
                            ],
                    ],
        ],
        'footerRowOptions' => [
            'class' => 'datatable-footer'
        ],
        'pager' => [
            'class' => '\yii\widgets\LinkPager',
            'disabledListItemSubTagOptions' => [
                'tag' => 'a',
                'class' => 'paginate-button'
            ],
            'activePageCssClass' => 'current',
            'maxButtonCount' => 5,
            'prevPageLabel' => '←',
            'prevPageCssClass' => 'previous',
            'nextPageLabel' => '→',
            'nextPageCssClass' => 'next',
            'linkContainerOptions' => [
                'tag' => 'li',
                'class' => 'paginate_button',
            ],
            'linkOptions' => [
                'class' => 'text-dark'
            ],
            'pageCssClass' => '',
            'options' => [
                'tag' => 'div',
                'class' => 'dataTables_paginate paging_simple_numbers'
            ]
        ]
    ]); ?>
    
    </div>

    <!-- Modal -->
    <div class="modal fade" id="list-modal" tabindex="-1" role="dialog" aria-labelledby="list-modal-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Biztosan törli a kiválasztott elemet?</h5>
                </div>
                <div class="modal-body">
                    Törlés megerősítésével véglegesen törli az elemet.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Mégsem</button>
                    <button type="button" class="btn btn-primary" data-url="/" id="delete-btn" onclick="window.location.href=$(this).data('url')">Törlés</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Pjax::end(); ?>
<?php
$this->registerJs(
    "
    $(document).on('pjax:success', function(e) {
        if (!$().fancybox) {
            console.warn('Warning - fancybox.min.js is not loaded.');
            return;
        }
        $('.popup-lightbox').fancybox({
            padding: 3
        });
    });
    ",
    $this::POS_READY,
    'fancybox-after-pjax'
);

/* if works as ck editor plugin */
if (!empty(Yii::$app->request->get('CKEditor'))){
    $this->registerJs("
        // Helper function to get parameters from the query string.
        function getUrlParam( paramName ) {
            var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' );
            var match = window.location.search.match( reParam );

            return ( match && match.length > 1 ) ? match[1] : null;
        }
        // Simulate user action of selecting a file to be returned to CKEditor.
        function returnFileUrl(fileId, fileUrl) {
            var funcNum = getUrlParam( 'CKEditorFuncNum' );
            //var fileUrl = '/path/to/file.txt';
            window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
            window.close();
        }
        ",
        $this::POS_END,
        'ckeditor-plugin'
    );
}
?>