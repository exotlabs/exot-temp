<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
?>

<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold"><?=Html::encode($this->title)?></span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <?php
        if (!empty($buttons)) {
        ?>
        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <?php
                foreach ($buttons as $button) {
                    $link = (isset($button['link']) ? $button['link'] : "");
                    $class = (isset($button['class']) ? $button['class'] : "");
                    $label = (isset($button['label']) ? $button['label'] : "");
                    $attributeText = '';
                    if (isset($button['attributes'])) {
                        foreach ($button['attributes'] as $idx => $attribute) {
                            $attributeText .= $idx."=\"".$attribute."\" ";
                        }
                    }
                    echo '<a class="btn btn-link btn-float text-default" '.$attributeText.' href="'.$link.'"><i class="'.$class.'  text-primary"></i><span>'.$label.'</span></a>';
                }
                ?>
            </div>
        </div>
        <?php
        }
        ?>

    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <?= Breadcrumbs::widget([
                'homeLink' => [ 
                    'label' => '<i class="icon-home2 mr-2"></i> Vezérlőpult',
                    'url' => '/admin/index',
                ],
                'encodeLabels' => false,
                'tag' =>'div',
                'options' => ['class' => 'breadcrumb'],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'activeItemTemplate'	=>'<span class="breadcrumb-item active">{link}</span>', 
                'itemTemplate'	=>'<span class="breadcrumb-item" >{link}</span>',
                ]) ?>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>